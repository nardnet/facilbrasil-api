CREATE TABLE arquivo (
	 codigo   BIGINT(20)  PRIMARY KEY AUTO_INCREMENT,
	 arquivo     VARCHAR(255),
	 tipo_inscricao VARCHAR(1),
	 numero_inscricao VARCHAR(18),
	 pagador     VARCHAR(255),
	 valor_titulo DECIMAL(10,2),
	 valor_pago DECIMAL(10,2),
	 valor_liquido DECIMAL(10,2),
	 error     VARCHAR(255),
	 data_importacao Date
)ENGINE=InnoDB DEFAULT CHARSET=utf8;