CREATE TABLE upload_arquivo (
	 codigo   BIGINT(20)  PRIMARY KEY AUTO_INCREMENT,
	 arquivo     VARCHAR(255),
     codigo_cliente BIGINT(20) NOT NULL,
	 data_importacao Date,
	 FOREIGN KEY (codigo_cliente) REFERENCES clientes(codigo)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;