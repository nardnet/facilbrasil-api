CREATE TABLE usuario (
	codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	usuario VARCHAR(50) NOT NULL,
	email VARCHAR(50) NOT NULL,
	senha VARCHAR(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE permissao (
	codigo BIGINT(20) PRIMARY KEY,
	descricao VARCHAR(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE usuario_permissao (
	codigo_usuario BIGINT(20) NOT NULL,
	codigo_permissao BIGINT(20) NOT NULL,
	PRIMARY KEY (codigo_usuario, codigo_permissao),
	FOREIGN KEY (codigo_usuario) REFERENCES usuario(codigo),
	FOREIGN KEY (codigo_permissao) REFERENCES permissao(codigo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO usuario (codigo, usuario, email, senha) values (1, 'Administrador', 'nardnet@gmail.com', '$2a$10$4elAI5K9Jj./nxQ8TXelZ.T7goqORi.EA4PNc0wy/T2ph/7kdACPy');
INSERT INTO usuario (codigo, usuario, email, senha) values (2, 'Rafael Campos', 'nardnet@gmail.com', '$2a$10$4elAI5K9Jj./nxQ8TXelZ.T7goqORi.EA4PNc0wy/T2ph/7kdACPy');

INSERT INTO permissao (codigo, descricao) values (1, 'ROLE_CADASTRAR_TITULO');
INSERT INTO permissao (codigo, descricao) values (2, 'ROLE_PESQUISAR_TITULO');
INSERT INTO permissao (codigo, descricao) values (3, 'ROLE_REMOVER_TITULO');

INSERT INTO permissao (codigo, descricao) values (4, 'ROLE_CADASTRAR_CLIENTE');
INSERT INTO permissao (codigo, descricao) values (5, 'ROLE_REMOVER_CLIENTE');
INSERT INTO permissao (codigo, descricao) values (6, 'ROLE_PESQUISAR_CLIENTE');


-- admin
INSERT INTO usuario_permissao (codigo_usuario, codigo_permissao) values (1, 1);
INSERT INTO usuario_permissao (codigo_usuario, codigo_permissao) values (1, 2);
INSERT INTO usuario_permissao (codigo_usuario, codigo_permissao) values (1, 3);
INSERT INTO usuario_permissao (codigo_usuario, codigo_permissao) values (1, 4);
INSERT INTO usuario_permissao (codigo_usuario, codigo_permissao) values (1, 5);
INSERT INTO usuario_permissao (codigo_usuario, codigo_permissao) values (1, 6);


-- Rafael
INSERT INTO usuario_permissao (codigo_usuario, codigo_permissao) values (2, 1);
INSERT INTO usuario_permissao (codigo_usuario, codigo_permissao) values (2, 6);
