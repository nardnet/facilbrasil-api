CREATE TABLE atendimento_contato (
    codigo  BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    nome  VARCHAR(255) NOT NULL,
    telefone VARCHAR(20) NOT NULL,
    cpf_cnpj VARCHAR (20) NOT NULL, 
    horario  VARCHAR (20) NOT NULL, 
    codigo_cliente  BIGINT(20) NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;