CREATE TABLE clientes (
    codigo   BIGINT(20)  PRIMARY KEY AUTO_INCREMENT,    
    nome     VARCHAR(255) NOT NULL,
    email   VARCHAR (20) NOT NULL,  
    cpf VARCHAR (20), 
    cnpj VARCHAR(18) NOT NULL,    
    rzo_social VARCHAR(255) NOT NULL,  
    insc_estadual  VARCHAR (20) NOT NULL,  
    endereco VARCHAR(50)NOT NULL,
    numero VARCHAR(30),
    bairro VARCHAR(50),
    cidade VARCHAR(50) NOT NULL, 
    codigo_estado BIGINT(20) NOT NULL,  
    cep VARCHAR(30) NOT NULL,
    telefone VARCHAR(20) NOT NULL,
    whatsapp VARCHAR(20) NOT NULL,
    str_atividade VARCHAR(1000) NOT NULL,
    princ_concorrentes VARCHAR(1000),
    ind_inadimplencia VARCHAR(1000),
    emp_group VARCHAR(1000),
    cont_cobranca VARCHAR(1000),
    cargo VARCHAR(255),
    dt_cadastro Date NOT NULL,
    observacoes VARCHAR(1000), 
    rep_nome     VARCHAR(255),
    rep_cpf VARCHAR (100),
    rep_email   VARCHAR (100),
    codigo_banco BIGINT(20) NOT NULL,
    agencia VARCHAR(30) NOT NULL,
    conta_corrente VARCHAR(30) NOT NULL,
    operacao VARCHAR(30),
    FOREIGN KEY (codigo_banco) REFERENCES bancos(codigo), 
	FOREIGN KEY (codigo_estado) REFERENCES estados(codigo)	 
)ENGINE=InnoDB DEFAULT CHARSET=utf8;