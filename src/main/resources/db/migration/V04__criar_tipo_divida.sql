CREATE TABLE tipo_divida (
	codigo BIGINT(20) PRIMARY KEY,
	tipo VARCHAR(50) NOT NULL	
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO tipo_divida (codigo, tipo) VALUES (1,'Duplicata');
INSERT INTO tipo_divida (codigo, tipo) VALUES (2,'Cheque');
INSERT INTO tipo_divida (codigo, tipo) VALUES (3,'Confissão de Divida');
INSERT INTO tipo_divida (codigo, tipo) VALUES (4,'Custas');
INSERT INTO tipo_divida (codigo, tipo) VALUES (5,'Fatura');
INSERT INTO tipo_divida (codigo, tipo) VALUES (6,'Outro');
INSERT INTO tipo_divida (codigo, tipo) VALUES (7,'Mensalidade');