package com.facilbrasil.api.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.facilbrasil.api.model.Clientes;
import com.facilbrasil.api.model.Titulos;
import com.facilbrasil.api.repository.ClientesRepository;
import com.facilbrasil.api.repository.TitulosRepository;
import com.facilbrasil.api.service.exception.ClienteInexistenteException;

@Service
public class TitulosService {
	
	@Autowired
	private ClientesRepository clientesRepository;
	
	@Autowired 
	private TitulosRepository titulosRepository;
	
	public Titulos salvar(Titulos titulo) {
		Clientes clientes = clientesRepository.findOne(titulo.getCliente().getCodigo());
		if (clientes == null) {
			throw new ClienteInexistenteException();
		}
		
		return titulosRepository.save(titulo);
		
	}
	

	
	public Titulos atualizar(Long codigo, Titulos lancamento) {
		Titulos tituloSalvo = buscarTitulosExistente(codigo);
		if (!lancamento.getCliente().equals(tituloSalvo.getCliente())) {
			validarPessoa(lancamento);
		}

		BeanUtils.copyProperties(lancamento, tituloSalvo , "codigo");

		return titulosRepository.save(tituloSalvo);
	}
	
	private void validarPessoa(Titulos titulo) {
		Clientes cliente = null;
		if (titulo.getCliente().getCodigo() != null) {
			cliente = clientesRepository.findOne(titulo.getCliente().getCodigo());
		}

		if (cliente == null) {
			throw new ClienteInexistenteException();
		}
	}

	private Titulos buscarTitulosExistente(Long codigo) {
		Titulos tituloSalvo = titulosRepository.findOne(codigo);
		if (tituloSalvo == null) {
			throw new IllegalArgumentException();
		}
		return tituloSalvo;
	}

	
}
