package com.facilbrasil.api.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.facilbrasil.api.dto.ContratoDTO;
import com.facilbrasil.api.mail.Mailer;
import com.facilbrasil.api.model.Clientes;
import com.facilbrasil.api.model.Titulos;
import com.facilbrasil.api.model.Usuario;
import com.facilbrasil.api.repository.ClientesRepository;
import com.facilbrasil.api.repository.UsuarioRepository;
import com.facilbrasil.api.service.exception.ClienteInexistenteException;
import com.facilbrasil.api.utils.PropertiesUtils;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import su.cmex.exportador.utils.xls.exporter.XLSGenerator;
import su.cmex.exportador.utils.xls.exporter.enumeration.CMEXCellTypeEnum;
import su.cmex.exportador.utils.xls.exporter.pojo.CMEXCellFont;
import su.cmex.exportador.utils.xls.exporter.pojo.CMEXCellStyle;
import su.cmex.exportador.utils.xls.exporter.pojo.CMEXRow;
import su.cmex.exportador.utils.xls.exporter.pojo.CMEXSheet;

@Service
public class ClientesService {
	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
	
	final String remetente = "REMETENTE";
	final String serverftp = "SERVERFTP";
	final String userftp = "USERFTP";
	final String passftp = "PASSFTP";
	final String dirftp = "DIRFTP";
	
	final String analista = "ANALISTA";
	final String erroFtpMail = "mail/erroFTP";
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private PropertiesUtils prop;
	
	@Autowired
	private Mailer mail;
	
	@PersistenceContext
	private EntityManager manager;
		
	@Autowired
	private ClientesRepository clientesRepository;
	
	
	public void atualizarPropriedadeAtivo(Long codigo, Boolean ativo) {
		Clientes clienteSalvo = buscarClientePeloCodigo(codigo);
		clienteSalvo.setAtivo(ativo);
		clientesRepository.save(clienteSalvo);
	}

	public Clientes atualizar(Long codigo, Clientes cliente) {
		Clientes clienteSalvo = clientesRepository.findOne(codigo);
		if (clienteSalvo == null) {
			throw new ClienteInexistenteException();
		}
		
			for (Usuario usuario : cliente.getUsuario()) {
				Usuario usuarioSalvo = null;
				String senha="";
				
				if (usuario.getCodigo() != null) {
					usuarioSalvo = usuarioRepository.findOne(usuario.getCodigo());
				}	
				
				if (usuarioSalvo == null) {
					usuario.setCliente(cliente);
					senha = usuario.getSenha();
					usuario.setLembrete(senha);	
					usuario.setSenha(null);
					usuario.setSenha(encoder.encode(senha));
				}
				else if ((usuarioSalvo.getLembrete()!= null && usuarioSalvo.getLembrete().equals(usuario.getSenha())) ||
					     (usuarioSalvo.getLembrete()!= null && usuario.getSenha().equals(usuarioSalvo.getSenha()))) {
					    senha = usuarioSalvo.getLembrete();					    
					    setaUsuario(cliente, usuario, usuarioSalvo, senha);	
					}else {
						senha = usuario.getSenha();						
						setaUsuario(cliente, usuario, usuarioSalvo, senha);	
					}	
			
					usuarioRepository.saveAndFlush(usuario);
			
		  
		}
		
		clienteSalvo.getUsuario().clear();
		clienteSalvo.getUsuario().addAll(cliente.getUsuario());
		clienteSalvo.getUsuario().forEach(t -> t.setCliente(clienteSalvo));

		clienteSalvo.getTitulos().clear();
		clienteSalvo.getTitulos().addAll(cliente.getTitulos());
		clienteSalvo.getTitulos().forEach(t -> t.setCliente(clienteSalvo));

		BeanUtils.copyProperties(cliente, clienteSalvo, "codigo", "titulos","usuario");
		

		
		return clientesRepository.save(clienteSalvo);
	}

	@SuppressWarnings("unlikely-arg-type")
	private void setaUsuario(Clientes cliente, Usuario usuario, Usuario usuarioSalvo, String senha) {
		usuario.setCliente(cliente);					
		usuario.setLembrete(senha);	
		usuario.setSenha(null);
		usuario.setSenha(encoder.encode(senha));
		usuarioSalvo.getPermissoes().remove(usuarioSalvo);
		usuarioRepository.delete(usuarioSalvo);
		usuarioRepository.flush();
	}
	
	public Clientes atualizarTitulos(Long codigo, Clientes cliente) {
		Clientes clienteSalvo = clientesRepository.findOne(codigo);
		if (clienteSalvo == null || clienteSalvo.isInativo()) {
			throw new ClienteInexistenteException();
		}

		  		
		clienteSalvo.getUsuario().clear();
		clienteSalvo.getUsuario().addAll(cliente.getUsuario());
		clienteSalvo.getUsuario().forEach(t -> t.setCliente(clienteSalvo));

		clienteSalvo.getTitulos().clear();
		clienteSalvo.getTitulos().addAll(cliente.getTitulos());
		clienteSalvo.getTitulos().forEach(t -> t.setCliente(clienteSalvo));

		BeanUtils.copyProperties(cliente, clienteSalvo, "codigo", "titulos", "usuario");
		

		
		return clientesRepository.save(clienteSalvo);
	}

	public Clientes buscarClientePeloCodigo(Long codigo) {
		Clientes clienteSalvo = clientesRepository.findOne(codigo);
		if (clienteSalvo == null) {
			throw new ClienteInexistenteException();
		}
		return clienteSalvo;
	}

	public Clientes salvar(Clientes cliente) {
		if(cliente.getTitulos()!=null)
		cliente.getTitulos().forEach(t -> t.setCliente(cliente));
		return clientesRepository.save(cliente);
	}
	
	public ByteArrayOutputStream gerarXls(Clientes cliente) {		
		List<CMEXSheet> sheets = new ArrayList<CMEXSheet>();
		CMEXSheet sheet = createSheetAll(cliente);
		sheets.add(sheet);
		XLSGenerator generator = new XLSGenerator();
		ByteArrayOutputStream output = (ByteArrayOutputStream) generator.generate(sheets);
		return output;

	}

	public void exportarXls(Clientes cliente) {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Date hoje = Calendar.getInstance().getTime();
		String dataAtual = df.format(hoje);
		
		Usuario usuario = usuarioRepository.findUsuarioPorCliente(cliente.getCodigo());
		
		List<CMEXSheet> sheets = new ArrayList<CMEXSheet>();
		CMEXSheet sheet = createSheet(cliente);
		sheets.add(sheet);
		XLSGenerator generator = new XLSGenerator();
		String server = prop.getLoadconfigBanco(serverftp);
		String user = prop.getLoadconfigBanco(userftp);
		String pass = prop.getLoadconfigBanco(passftp);
		//String diretorio = prop.getLoadconfigBanco(dirftp);
		String arquivo =  usuario.getUsuario() + dataAtual.replaceAll("/", "") + ".xls";

		try {
			ByteArrayOutputStream output = (ByteArrayOutputStream) generator.generate(sheets);

			FTPClient ftp = new FTPClient();
			ftp.connect(server);
			
			 System.out.print(ftp.getReplyString());
			 
			if (!FTPReply.isPositiveCompletion(ftp.getReplyCode())) {
				    ftp.disconnect();
	                System.out.println("Connection refused.");
	           	    Map<String, Object> variaveis = new HashMap<>();
			 		variaveis.put("clienteSalvo", cliente);		 		
			 		
			 		mail.enviarEmail(prop.getLoadconfigBanco(remetente), 
			 				Arrays.asList(prop.getLoadconfigBanco(remetente),prop.getLoadconfigBanco(analista)), 
			 				"Alerta! Não foi possivel acessar o ftp para o cliente "+ cliente.getNome(), erroFtpMail, variaveis);
			 		System.out.println("Terminado o envio de e-mail...");
	                return;
	        }
			ftp.login(user, pass);
			
			
			ftp.enterLocalPassiveMode();
			
			System.out.println("user " + user);
			
			System.out.println("pass " +pass);
			
			System.out.println("arquivo " + arquivo);
			
			//ftp.changeWorkingDirectory(diretorio);
			ftp.setFileType(FTPClient.BINARY_FILE_TYPE);
			OutputStream os = ftp.storeFileStream(arquivo);	

			os.write(output.toByteArray());
			os.flush();
			os.close();
			
	        System.out.println("getStatus "+ftp.getStatus("/www"));
	        System.out.println("getSystemName "+ftp.getSystemType());
	        System.out.println("listHelp "+ftp.listHelp());
	        System.out.println("listNames "+ftp.listNames());	        
	      
			ftp.logout();		
			ftp.disconnect();

		}  catch (ConnectException e) {
			 System.out.println("Falha de conexao!");
			 Map<String, Object> variaveis = new HashMap<>();
		 		variaveis.put("clienteSalvo", cliente);		 		
		 		
		 		mail.enviarEmail(prop.getLoadconfigBanco(remetente), 
		 				Arrays.asList(prop.getLoadconfigBanco(remetente),prop.getLoadconfigBanco(analista)), 
		 				"Alerta! Não foi possivel acessar o ftp para o cliente "+ cliente.getNome(), erroFtpMail, variaveis);
		 		System.out.println("Terminado o envio de e-mail...");
			
				e.printStackTrace();
		}	
		 catch (IOException e) {
			  System.out.println("Dentro da exception");
			e.printStackTrace();
		}

	}

	private CMEXSheet createSheet(Clientes cliente) {		
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Date hoje = Calendar.getInstance().getTime();
		String dataAtual = df.format(hoje);
				
		String tituloPlan = "";
		String ddd = "";
		String telefone = "";
		tituloPlan = "Planilha";
		CMEXSheet sheet = new CMEXSheet(tituloPlan);
		this.fillSheetHeader(sheet);
		
		
		Usuario usuario = usuarioRepository.findUsuarioPorCliente(cliente.getCodigo());
		
		for (Titulos titulo : cliente.getTitulos()) {
			String dataEnvio = df.format(titulo.getDataEnvioEntrada());
			
			if (dataAtual.equals(dataEnvio)) {
			
			CMEXRow row = sheet.addRow();
			
			row.addCell(titulo.getNome().toUpperCase(), CMEXCellTypeEnum.STRING);
			row.addCell(titulo.getCpfCnpj(), CMEXCellTypeEnum.STRING);
			row.addCell(titulo.getContratoConta(), CMEXCellTypeEnum.STRING);
			row.addCell(String.valueOf(titulo.getTipoDivida()), CMEXCellTypeEnum.STRING);
			if (titulo.getTipoDivida().getCodigo() == 2) {
				row.addCell(" Nº " + titulo.getNumOuTituloCheque() +
						" BCO "  + titulo.getBanco().getBanco().substring(0,3) + 
						" AGO " + titulo.getAgencia() + 
						" C/C " + titulo.getContaCorrente(), CMEXCellTypeEnum.STRING);
			}else {
				row.addCell(titulo.getNumOuTituloCheque(), CMEXCellTypeEnum.STRING);	
			}				
			row.addCell(titulo.getDataEnvioEntrada(), CMEXCellTypeEnum.DATE);
			row.addCell(titulo.getDataVencimentoDivida(), CMEXCellTypeEnum.DATE);
			
			NumberFormat nf = NumberFormat.getInstance();
		    nf.setMaximumFractionDigits(2);
			String saldo = null;
			
			
			if (titulo.getSaldoACobrar() != null) {				
				saldo = titulo.getSaldoACobrar().setScale(2).toString();
				saldo = saldo.replace(".", ",");
			}
				
			row.addCell(saldo != null ?  saldo :  "0,00" , CMEXCellTypeEnum.STRING);
			row.addCell(saldo != null ?  saldo :  "0,00" , CMEXCellTypeEnum.STRING);
			row.addCell(titulo.getEndereco() != null ? titulo.getEndereco().toUpperCase(): "", CMEXCellTypeEnum.STRING);
			row.addCell(titulo.getNumero() != null ? titulo.getNumero().toUpperCase(): "", CMEXCellTypeEnum.STRING);
			row.addCell(titulo.getComplemento() != null ? titulo.getComplemento().toUpperCase(): "", CMEXCellTypeEnum.STRING);
			row.addCell(titulo.getBairro() != null ? titulo.getBairro().toUpperCase() : "", CMEXCellTypeEnum.STRING);
			row.addCell(titulo.getMunicipio() != null ? titulo.getMunicipio().toUpperCase(): "", CMEXCellTypeEnum.STRING);
			row.addCell(titulo.getEstado() !=null ? titulo.getEstado().getUf().toUpperCase():"", CMEXCellTypeEnum.STRING);
			row.addCell(titulo.getCep() != null ? titulo.getCep().replaceAll("[^0-9]", "") : "", CMEXCellTypeEnum.STRING);
		
			if (titulo.getTelefone1() != null && !titulo.getTelefone1().isEmpty()) {
				if (titulo.getTelefone1().length() > 3)
				ddd =   titulo.getTelefone1().substring(1,3);
				row.addCell(ddd, CMEXCellTypeEnum.STRING);
			}else {
				row.addCell("", CMEXCellTypeEnum.STRING);
			}if (titulo.getTelefone1() != null && !titulo.getTelefone1().isEmpty()) {
				if (titulo.getTelefone1().length() > 3)
				telefone =   titulo.getTelefone1().substring(4,titulo.getTelefone1().length());
				row.addCell(telefone.replaceAll("[^0-9]", ""), CMEXCellTypeEnum.STRING);
			}else {
				row.addCell("", CMEXCellTypeEnum.STRING);
			}
			
			ddd = "";
			telefone = "";
			
			if (titulo.getTelefone2() != null && !titulo.getTelefone2().isEmpty()) {
				if (titulo.getTelefone2().length() > 3)
				ddd =   titulo.getTelefone2().substring(1,3);
				row.addCell(ddd, CMEXCellTypeEnum.STRING);
			}else {
				row.addCell("", CMEXCellTypeEnum.STRING);
			}if (titulo.getTelefone2() != null && !titulo.getTelefone2().isEmpty()) {
				if (titulo.getTelefone2().length() > 3)
				telefone =   titulo.getTelefone2().substring(4,titulo.getTelefone2().length());
				row.addCell(telefone.replaceAll("[^0-9]", ""), CMEXCellTypeEnum.STRING);
			}else {
				row.addCell("", CMEXCellTypeEnum.STRING);
			}
			
			ddd = "";
			telefone = "";
	
			if (titulo.getCelular() != null && !titulo.getCelular().isEmpty()) {
				if (titulo.getCelular().length() > 3)
				ddd =   titulo.getCelular().substring(1,3);
				row.addCell(ddd, CMEXCellTypeEnum.STRING);
			}else {
				row.addCell("", CMEXCellTypeEnum.STRING);
			}if (titulo.getCelular() != null && !titulo.getCelular().isEmpty()) {
				if (titulo.getCelular().length() > 3)
				telefone =   titulo.getCelular().substring(4,titulo.getCelular().length());
				row.addCell(telefone.replaceAll("[^0-9]", ""), CMEXCellTypeEnum.STRING);
			}else {
				row.addCell("", CMEXCellTypeEnum.STRING);
			}
	
			
			row.addCell(titulo.getReferencia() != null ? titulo.getReferencia().toUpperCase() : "", CMEXCellTypeEnum.STRING);
			row.addCell(usuario.getLembrete() != null ? usuario.getLembrete().toUpperCase() : "", CMEXCellTypeEnum.STRING);
			row.addCell(titulo.getEmail() != null ? titulo.getEmail(): "", CMEXCellTypeEnum.STRING);
			row.addCell("CARTEIRA", CMEXCellTypeEnum.STRING);
		   }
		}

		sheet.setFreeze(0, 1);
		// sheet.setAutoSize(false);
		return sheet;
	}
	
	
	private CMEXSheet createSheetAll(Clientes cliente) {		
		String tituloPlan = "";
		String ddd = "";
		String telefone = "";
		tituloPlan = "Planilha";
		CMEXSheet sheet = new CMEXSheet(tituloPlan);
		this.fillSheetHeader(sheet);
		
		
		Usuario usuario = usuarioRepository.findUsuarioPorCliente(cliente.getCodigo());
		
		for (Titulos titulo : cliente.getTitulos()) {
			
			CMEXRow row = sheet.addRow();
			
			row.addCell(titulo.getNome().toUpperCase(), CMEXCellTypeEnum.STRING);
			row.addCell(titulo.getCpfCnpj(), CMEXCellTypeEnum.STRING);
			row.addCell(titulo.getContratoConta(), CMEXCellTypeEnum.STRING);
			row.addCell(String.valueOf(titulo.getTipoDivida()), CMEXCellTypeEnum.STRING);
			if (titulo.getTipoDivida().getCodigo() == 2) {
				row.addCell(" Nº " + titulo.getNumOuTituloCheque() +
						" BCO "  + titulo.getBanco().getBanco().substring(0,3) + 
						" AGO " + titulo.getAgencia() + 
						" C/C " + titulo.getContaCorrente(), CMEXCellTypeEnum.STRING);
			}else {
				row.addCell(titulo.getNumOuTituloCheque(), CMEXCellTypeEnum.STRING);	
			}				
			row.addCell(titulo.getDataEnvioEntrada(), CMEXCellTypeEnum.DATE);
			row.addCell(titulo.getDataVencimentoDivida(), CMEXCellTypeEnum.DATE);
			
			NumberFormat nf = NumberFormat.getInstance();
		    nf.setMaximumFractionDigits(2);
			String saldo = null;
			
			
			if (titulo.getSaldoACobrar() != null) {				
				saldo = titulo.getSaldoACobrar().setScale(2).toString();
				saldo = saldo.replace(".", ",");
			}
				
			row.addCell(saldo != null ?  saldo :  "0,00" , CMEXCellTypeEnum.STRING);
			row.addCell(saldo != null ?  saldo :  "0,00" , CMEXCellTypeEnum.STRING);
			row.addCell(titulo.getEndereco() != null ? titulo.getEndereco().toUpperCase(): "", CMEXCellTypeEnum.STRING);
			row.addCell(titulo.getNumero() != null ? titulo.getNumero().toUpperCase(): "", CMEXCellTypeEnum.STRING);
			row.addCell(titulo.getComplemento() != null ? titulo.getComplemento().toUpperCase(): "", CMEXCellTypeEnum.STRING);
			row.addCell(titulo.getBairro() != null ? titulo.getBairro().toUpperCase() : "", CMEXCellTypeEnum.STRING);
			row.addCell(titulo.getMunicipio() != null ? titulo.getMunicipio().toUpperCase(): "", CMEXCellTypeEnum.STRING);
			row.addCell(titulo.getEstado() !=null ? titulo.getEstado().getUf().toUpperCase():"", CMEXCellTypeEnum.STRING);
			row.addCell(titulo.getCep() != null ? titulo.getCep().replaceAll("[^0-9]", "") : "", CMEXCellTypeEnum.STRING);
		
			if (titulo.getTelefone1() != null && !titulo.getTelefone1().isEmpty()) {
				if (titulo.getTelefone1().length() > 3)
				ddd =   titulo.getTelefone1().substring(1,3);
				row.addCell(ddd, CMEXCellTypeEnum.STRING);
			}else {
				row.addCell("", CMEXCellTypeEnum.STRING);
			}if (titulo.getTelefone1() != null && !titulo.getTelefone1().isEmpty()) {
				if (titulo.getTelefone1().length() > 3)
				telefone =   titulo.getTelefone1().substring(4,titulo.getTelefone1().length());
				row.addCell(telefone.replaceAll("[^0-9]", ""), CMEXCellTypeEnum.STRING);
			}else {
				row.addCell("", CMEXCellTypeEnum.STRING);
			}
			
			ddd = "";
			telefone = "";
			
			if (titulo.getTelefone2() != null && !titulo.getTelefone2().isEmpty()) {
				if (titulo.getTelefone2().length() > 3)
				ddd =   titulo.getTelefone2().substring(1,3);
				row.addCell(ddd, CMEXCellTypeEnum.STRING);
			}else {
				row.addCell("", CMEXCellTypeEnum.STRING);
			}if (titulo.getTelefone2() != null && !titulo.getTelefone2().isEmpty()) {
				if (titulo.getTelefone2().length() > 3)
				telefone =   titulo.getTelefone2().substring(4,titulo.getTelefone2().length());
				row.addCell(telefone.replaceAll("[^0-9]", ""), CMEXCellTypeEnum.STRING);
			}else {
				row.addCell("", CMEXCellTypeEnum.STRING);
			}
			
			ddd = "";
			telefone = "";
	
			if (titulo.getCelular() != null && !titulo.getCelular().isEmpty()) {
				if (titulo.getCelular().length() > 3)
				ddd =   titulo.getCelular().substring(1,3);
				row.addCell(ddd, CMEXCellTypeEnum.STRING);
			}else {
				row.addCell("", CMEXCellTypeEnum.STRING);
			}if (titulo.getCelular() != null && !titulo.getCelular().isEmpty()) {
				if (titulo.getCelular().length() > 3)
				telefone =   titulo.getCelular().substring(4,titulo.getCelular().length());
				row.addCell(telefone.replaceAll("[^0-9]", ""), CMEXCellTypeEnum.STRING);
			}else {
				row.addCell("", CMEXCellTypeEnum.STRING);
			}
	
			
			row.addCell(titulo.getReferencia() != null ? titulo.getReferencia().toUpperCase() : "", CMEXCellTypeEnum.STRING);
			row.addCell(usuario.getLembrete() != null ? usuario.getLembrete().toUpperCase() : "", CMEXCellTypeEnum.STRING);
			row.addCell(titulo.getEmail() != null ? titulo.getEmail(): "", CMEXCellTypeEnum.STRING);
			row.addCell("CARTEIRA", CMEXCellTypeEnum.STRING);
		   }
		

		sheet.setFreeze(0, 1);
		// sheet.setAutoSize(false);
		return sheet;
	}


	
	private void fillSheetHeader(CMEXSheet sheet) {
		CMEXRow header = sheet.addRow();

		CMEXCellFont headerFont = new CMEXCellFont();
		headerFont.setFontHeightInPoints(10);
		headerFont.setBold(true);

		CMEXCellStyle headerStyle = new CMEXCellStyle();
		headerStyle.setFillForeground(IndexedColors.LAVENDER);
		headerStyle.setFont(headerFont);

		header.addCell("NOME", headerStyle, CMEXCellTypeEnum.STRING);
		header.addCell("CPF / CNPJ", headerStyle, CMEXCellTypeEnum.STRING);
		header.addCell("CONTRATO/CONTA", headerStyle, CMEXCellTypeEnum.STRING);
		header.addCell("DOCUMENTO OU TIPO DE DÍVIDA", headerStyle, CMEXCellTypeEnum.STRING);
		header.addCell("NÚMERO DO TITULO/CHEQUE", headerStyle, CMEXCellTypeEnum.STRING);
		header.addCell("DATA DE ENVIO/ENTRADA", headerStyle, CMEXCellTypeEnum.STRING);
		header.addCell("VENCIMENTO DÍVIDA", headerStyle, CMEXCellTypeEnum.STRING);
		header.addCell("VALOR ORIGINAL", headerStyle, CMEXCellTypeEnum.STRING);		
		header.addCell("SALDO A COBRAR", headerStyle, CMEXCellTypeEnum.STRING);
		header.addCell("ENDEREÇO", headerStyle, CMEXCellTypeEnum.STRING);
		header.addCell("NÚMERO", headerStyle, CMEXCellTypeEnum.STRING);
		header.addCell("COMPLEMENTO", headerStyle, CMEXCellTypeEnum.STRING);
		header.addCell("BAIRRO", headerStyle, CMEXCellTypeEnum.STRING);
		header.addCell("MUNICÍPIO", headerStyle, CMEXCellTypeEnum.STRING);
		header.addCell("ESTADO", headerStyle, CMEXCellTypeEnum.STRING);
		header.addCell("CEP", headerStyle, CMEXCellTypeEnum.STRING);
		header.addCell("DDD", headerStyle, CMEXCellTypeEnum.STRING);
		header.addCell("TELEFONE 1", headerStyle, CMEXCellTypeEnum.STRING);
		header.addCell("DDD", headerStyle, CMEXCellTypeEnum.STRING);
		header.addCell("TELEFONE 2", headerStyle, CMEXCellTypeEnum.STRING);
		header.addCell("DDD", headerStyle, CMEXCellTypeEnum.STRING);
		header.addCell("CELULAR", headerStyle, CMEXCellTypeEnum.STRING);
		header.addCell("REFERENCIA", headerStyle, CMEXCellTypeEnum.STRING);
		header.addCell("CLASSE / LOJA/SETOR / SEMESTRE-ANO", headerStyle, CMEXCellTypeEnum.STRING);
		header.addCell("EMAIL", headerStyle, CMEXCellTypeEnum.STRING);
		header.addCell("DESCRICAO", headerStyle, CMEXCellTypeEnum.STRING);

	}
	
	public byte[] relatorioContrato(Clientes cliente) throws Exception {
		List<ContratoDTO> listContrato = new ArrayList<ContratoDTO>();
		Map<String, Object> paramentros = new HashMap<>();
		paramentros.put("IMAGE", this.getClass().getResourceAsStream("/image/imagemFacil.png"));
		paramentros.put("IMAGE2", this.getClass().getResourceAsStream("/image/controimg1.png"));
		paramentros.put("IMAGE3", this.getClass().getResourceAsStream("/image/controimg2.png"));
		ContratoDTO contrato = new ContratoDTO();
		contrato.setAgencia(cliente.getAgencia());
		contrato.setBairro(cliente.getEndereco().getBairro());
		contrato.setBanco(cliente.getBanco().getBanco());
		contrato.setCep(cliente.getEndereco().getCep());
		contrato.setCidade(cliente.getEndereco().getCidade());
		contrato.setCnpj(cliente.getCnpj());
		contrato.setContaCorrente(cliente.getContaCorrente());
		contrato.setEndereco(cliente.getEndereco().getEndereco());
		contrato.setNumero(cliente.getEndereco().getNumero());
		contrato.setRzoSocial(cliente.getRzoSocial());
		contrato.setUf(cliente.getEndereco().getEstado().getUf());
		
		
		listContrato.add(contrato);
		
		InputStream inputStream = this.getClass().getResourceAsStream(
				"/relatorios/contrato.jasper");

		
		JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream, paramentros,
				new JRBeanCollectionDataSource(listContrato) );
		
		return JasperExportManager.exportReportToPdf(jasperPrint);
	}
	

}
