package com.facilbrasil.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.facilbrasil.api.model.Usuario;
import com.facilbrasil.api.repository.UsuarioRepository;

@Service
public class UsuarioService {
	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	public Usuario atualizar(Long codigo, Usuario usuario) {
		Usuario usuarioSalvo = usuarioRepository.findOne(codigo);
		if (usuarioSalvo == null) {
			throw new EmptyResultDataAccessException(1);
		}		
		usuarioRepository.delete(usuarioSalvo);
		usuarioRepository.flush();		
		
		String senha = usuario.getSenha();
		usuario.setLembrete(senha);	
		usuario.setSenha(encoder.encode(senha));  

		return usuarioRepository.save(usuario);
	}
	

}
