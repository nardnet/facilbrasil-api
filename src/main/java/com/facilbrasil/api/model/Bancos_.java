package com.facilbrasil.api.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Bancos.class)
public abstract class Bancos_ {

	public static volatile SingularAttribute<Bancos, Long> codigo;
	public static volatile SingularAttribute<Bancos, String> banco;

}

