package com.facilbrasil.api.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UploadArquivo.class)
public abstract class UploadArquivo_ {

	public static volatile SingularAttribute<UploadArquivo, Clientes> cliente;
	public static volatile SingularAttribute<UploadArquivo, Long> codigo;
	public static volatile SingularAttribute<UploadArquivo, BigDecimal> saldoACobrar;
	public static volatile SingularAttribute<UploadArquivo, String> arquivo;
	public static volatile SingularAttribute<UploadArquivo, Date> dataImportacao;
	public static volatile SingularAttribute<UploadArquivo, Long> quantidade;

}

