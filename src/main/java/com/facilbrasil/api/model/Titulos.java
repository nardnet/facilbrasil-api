package com.facilbrasil.api.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;

@Entity
@Table(name = "titulos")
public class Titulos {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;
	
	
	@ManyToOne
	@JoinColumn(name = "codigo_cliente")
	private Clientes cliente;
	
	@NotNull
	private String nome;
	
	@NotNull
	private String cpfCnpj;
	
	@NotNull
	private String contratoConta;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "codigo_tpdivida")
	private TipoDivida tipoDivida;	
	@NotNull
	private String numOuTituloCheque;
	
	@ManyToOne
	@JoinColumn(name = "codigo_banco")
	private Bancos banco;
	
	private String agencia;
	
	private String contaCorrente;


	@NotNull
	private Date dataEnvioEntrada;	

	private Date dataVencimentoDivida;

	private BigDecimal valorOriginal;
	
	@Column(name = "saldo_a_cobrar")
	private BigDecimal saldoACobrar;

	private String endereco;
	private String numero;
	private String complemento;
	private String bairro;
	private String municipio;	
	
	@ManyToOne
	@JoinColumn(name = "codigo_estado")
	private Estados estado;
	
	private String cep;	
	private String telefone1;	
	private String telefone2;
	private String celular;

	private String referencia;

	private String classeLojaSetor;

	@Email
	private String email;

	private String descricao;

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public Clientes getCliente() {
		return cliente;
	}

	public void setCliente(Clientes cliente) {
		this.cliente = cliente;
	}

	public String getNome() {
		return nome != null ? nome.toUpperCase() : nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpfCnpj() {
		return cpfCnpj != null ? cpfCnpj.toUpperCase() : cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getContratoConta() {
		return contratoConta != null ? contratoConta.toUpperCase() : contratoConta;
	}

	public void setContratoConta(String contratoConta) {
		this.contratoConta = contratoConta;
	}

	public TipoDivida getTipoDivida() {
		return tipoDivida;
	}

	public void setTipoDivida(TipoDivida tipoDivida) {
		this.tipoDivida = tipoDivida;
	}


	public Bancos getBanco() {
		return banco;
	}

	public void setBanco(Bancos banco) {
		this.banco = banco;
	}

	public String getAgencia() {
		return agencia != null ? agencia.toUpperCase() : agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getContaCorrente() {
		return contaCorrente != null ? contaCorrente.toUpperCase() : contaCorrente;
	}

	public void setContaCorrente(String contaCorrente) {
		this.contaCorrente = contaCorrente;
	}

	public String getNumOuTituloCheque() {
		return numOuTituloCheque != null ? numOuTituloCheque.toUpperCase() : numOuTituloCheque;
	}

	public void setNumOuTituloCheque(String numOuTituloCheque) {
		this.numOuTituloCheque = numOuTituloCheque;
	}

	public Date getDataEnvioEntrada() {
		return dataEnvioEntrada;
	}

	public void setDataEnvioEntrada(Date dataEnvioEntrada) {
		this.dataEnvioEntrada = dataEnvioEntrada;
	}

	public Date getDataVencimentoDivida() {
		return dataVencimentoDivida;
	}

	public void setDataVencimentoDivida(Date dataVencimentoDivida) {
		this.dataVencimentoDivida = dataVencimentoDivida;
	}

	public BigDecimal getValorOriginal() {
		return valorOriginal;
	}

	public void setValorOriginal(BigDecimal valorOriginal) {
		this.valorOriginal = valorOriginal;
	}

	public BigDecimal getSaldoACobrar() {
		return saldoACobrar;
	}

	public void setSaldoACobrar(BigDecimal saldoACobrar) {
		this.saldoACobrar = saldoACobrar;
	}

	public String getEndereco() {
		return endereco != null ? endereco.toUpperCase() : endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNumero() {
		return numero != null ? numero.toUpperCase() : numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento != null ? complemento.toUpperCase() : complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro != null ? bairro.toUpperCase() : bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getMunicipio() {
		return municipio != null ? municipio.toUpperCase() : municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public Estados getEstado() {
		return estado;
	}

	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	public String getCep() {
		return cep != null ? cep.toUpperCase() : cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getTelefone1() {
		return telefone1 != null ? telefone1.toUpperCase() : telefone1;
	}

	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}

	public String getTelefone2() {
		return telefone2 !=null ? telefone2.toUpperCase() : telefone2;
	}

	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}

	public String getCelular() {
		return celular != null ? celular.toUpperCase() : celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getReferencia() {
		return referencia != null ? referencia.toUpperCase() : referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getClasseLojaSetor() {
		return classeLojaSetor != null ? classeLojaSetor.toUpperCase() : classeLojaSetor;
	}

	public void setClasseLojaSetor(String classeLojaSetor) {
		this.classeLojaSetor = classeLojaSetor;
	}

	public String getEmail() {
		return email != null ? email.toUpperCase() : email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDescricao() {
		return descricao != null ? descricao.toUpperCase() : descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Titulos other = (Titulos) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}

	
}
