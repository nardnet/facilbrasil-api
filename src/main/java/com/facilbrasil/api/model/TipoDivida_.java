package com.facilbrasil.api.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TipoDivida.class)
public abstract class TipoDivida_ {

	public static volatile SingularAttribute<TipoDivida, Long> codigo;
	public static volatile SingularAttribute<TipoDivida, String> tipo;

}

