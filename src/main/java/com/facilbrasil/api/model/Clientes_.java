package com.facilbrasil.api.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Clientes.class)
public abstract class Clientes_ {

	public static volatile SingularAttribute<Clientes, String> inscEstadual;
	public static volatile SingularAttribute<Clientes, String> whatsapp;
	public static volatile SingularAttribute<Clientes, String> telefone;
	public static volatile SingularAttribute<Clientes, Boolean> ativo;
	public static volatile ListAttribute<Clientes, Titulos> titulos;
	public static volatile ListAttribute<Clientes, UploadArquivo> uploadArquivos;
	public static volatile SingularAttribute<Clientes, String> repEmail;
	public static volatile SingularAttribute<Clientes, String> contCobranca;
	public static volatile SingularAttribute<Clientes, String> cnpj;
	public static volatile SingularAttribute<Clientes, String> empGroup;
	public static volatile SingularAttribute<Clientes, String> agencia;
	public static volatile SingularAttribute<Clientes, String> cpf;
	public static volatile SingularAttribute<Clientes, String> cargo;
	public static volatile SingularAttribute<Clientes, String> strAtividade;
	public static volatile SingularAttribute<Clientes, String> email;
	public static volatile SingularAttribute<Clientes, Long> codigo;
	public static volatile SingularAttribute<Clientes, Endereco> endereco;
	public static volatile SingularAttribute<Clientes, Date> dtCadastro;
	public static volatile SingularAttribute<Clientes, Bancos> banco;
	public static volatile SingularAttribute<Clientes, String> nome;
	public static volatile SingularAttribute<Clientes, String> indInadimplencia;
	public static volatile SingularAttribute<Clientes, String> observacoes;
	public static volatile SingularAttribute<Clientes, String> repCpf;
	public static volatile SingularAttribute<Clientes, String> operacao;
	public static volatile SingularAttribute<Clientes, String> contaCorrente;
	public static volatile SingularAttribute<Clientes, String> princConcorrentes;
	public static volatile SingularAttribute<Clientes, String> repNome;
	public static volatile ListAttribute<Clientes, Usuario> usuario;
	public static volatile SingularAttribute<Clientes, String> rzoSocial;

}

