package com.facilbrasil.api.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Estados.class)
public abstract class Estados_ {

	public static volatile SingularAttribute<Estados, String> uf;
	public static volatile SingularAttribute<Estados, Long> codigo;
	public static volatile SingularAttribute<Estados, String> nome;
	public static volatile SingularAttribute<Estados, Long> regiao;
	public static volatile SingularAttribute<Estados, Long> codUf;

}

