package com.facilbrasil.api.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "clientes")
public class Clientes {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;
	
	@NotNull
	private String nome;
	
	@NotNull
	@Email
	private String email;
	
	private String cpf;
	
	@NotNull
	private String rzoSocial;
	
	@NotNull
	private String cnpj;
	
	@NotNull
	private String inscEstadual;
	
	@Embedded
	private Endereco endereco;
	
	@NotNull
	private String telefone;
	
	@NotNull
	private String whatsapp;
	
	@NotNull
	private String strAtividade;
	
	private String princConcorrentes;
	
	private String indInadimplencia;
	
	private String empGroup;
	
	private String contCobranca;
	
	private String cargo;
	
	private String repNome;
	
	private String repCpf;
	
	@Email
	private String repEmail;
	
	private String observacoes;

	@JsonIgnoreProperties("cliente")
	@Valid
	@OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL ,
			orphanRemoval = true)
	private List<Titulos> titulos;	
	
	@JsonIgnoreProperties("cliente")
	@Valid
	@OneToMany(mappedBy = "cliente")
	private List<UploadArquivo> uploadArquivos;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "codigo_banco")
	private Bancos banco;

	@NotNull
	private String agencia;
	@NotNull
	private String contaCorrente;

	private String operacao;

	@NotNull
	private Date dtCadastro;
	
	
	private Boolean ativo;
	
	
	@JsonIgnoreProperties("cliente")
	@Valid
	@OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL ,
			orphanRemoval = true)
	private List<Usuario> usuario;
		

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome != null ? nome.toUpperCase() : nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email != null ? email.toUpperCase() : email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCpf() {
		return cpf != null ? cpf.toUpperCase() : cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRzoSocial() {
		return rzoSocial != null ? rzoSocial.toUpperCase() : rzoSocial;
	}

	public void setRzoSocial(String rzoSocial) {
		this.rzoSocial = rzoSocial;
	}

	public String getCnpj() {
		return cnpj  != null ? cnpj.toUpperCase() : cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getInscEstadual() {
		return inscEstadual != null ? inscEstadual.toUpperCase() : inscEstadual;
	}

	public void setInscEstadual(String inscEstadual) {
		this.inscEstadual = inscEstadual;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String getTelefone() {
		return telefone != null ? telefone.toUpperCase() : telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getWhatsapp() {
		return whatsapp != null ? whatsapp.toUpperCase() : whatsapp;
	}

	public void setWhatsapp(String whatsapp) {
		this.whatsapp = whatsapp;
	}

	public String getStrAtividade() {
		return strAtividade != null ? strAtividade.toUpperCase() : strAtividade ;
	}

	public void setStrAtividade(String strAtividade) {
		this.strAtividade = strAtividade;
	}

	public String getPrincConcorrentes() {
		return princConcorrentes !=null ? princConcorrentes.toUpperCase() : princConcorrentes ;
	}

	public void setPrincConcorrentes(String princConcorrentes) {
		this.princConcorrentes = princConcorrentes;
	}

	public String getIndInadimplencia() {
		return indInadimplencia != null ? indInadimplencia.toUpperCase() : indInadimplencia;
	}

	public void setIndInadimplencia(String indInadimplencia) {
		this.indInadimplencia = indInadimplencia;
	}

	public String getEmpGroup() {
		return empGroup != null ? empGroup.toUpperCase() : empGroup;
	}

	public void setEmpGroup(String empGroup) {
		this.empGroup = empGroup;
	}

	public String getContCobranca() {
		return contCobranca != null ? contCobranca.toUpperCase() : contCobranca;
	}

	public void setContCobranca(String contCobranca) {
		this.contCobranca = contCobranca;
	}

	public String getCargo() {
		return cargo != null ? cargo.toUpperCase() : cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getObservacoes() {
		return observacoes != null ? observacoes.toUpperCase() : observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

	public Bancos getBanco() {
		return banco;
	}

	public void setBanco(Bancos banco) {
		this.banco = banco;
	}

	public String getAgencia() {
		return agencia != null ? agencia.toUpperCase() : agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getOperacao() {
		return operacao != null ? operacao.toUpperCase() : operacao ;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}

	public String getContaCorrente() {
		return contaCorrente != null ? contaCorrente.toUpperCase() : contaCorrente;
	}

	public void setContaCorrente(String contaCorrente) {
		this.contaCorrente = contaCorrente;
	}

	public Date getDtCadastro() {
		return dtCadastro;
	}

	public void setDtCadastro(Date dtCadastro) {
		this.dtCadastro = dtCadastro;
	}

	public List<Titulos> getTitulos() {
		return titulos;
	}

	public void setTitulos(List<Titulos> titulos) {
		this.titulos = titulos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Clientes other = (Clientes) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return this.nome;
	}

	public String getRepNome() {
		return repNome != null ?  repNome.toUpperCase() : repNome;
	}

	public void setRepNome(String repNome) {
		this.repNome = repNome;
	}

	public String getRepCpf() {
		return repCpf != null ? repCpf.toUpperCase() : repCpf;
	}

	public void setRepCpf(String repCpf) {
		this.repCpf = repCpf;
	}

	public String getRepEmail() {
		return repEmail != null ? repEmail.toUpperCase() : repEmail;
	}

	public void setRepEmail(String repEmail) {
		this.repEmail = repEmail;
	}

	public List<Usuario> getUsuario() {
		return usuario;
	}

	public void setUsuario(List<Usuario> usuario) {
		this.usuario = usuario == null ? new ArrayList<Usuario>() : usuario;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	@JsonIgnore
	@Transient
	public boolean isInativo() {
		return !this.ativo;
	}

	public List<UploadArquivo> getUploadArquivos() {
		return uploadArquivos;
	}

	public void setUploadArquivos(List<UploadArquivo> uploadArquivos) {
		this.uploadArquivos = uploadArquivos;
	}
	
	

}
