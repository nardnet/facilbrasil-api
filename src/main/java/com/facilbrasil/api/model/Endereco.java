package com.facilbrasil.api.model;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
@Embeddable
public class Endereco {
	
	@NotNull
	private String endereco;
	private String numero;
	private String bairro;
	@NotNull
	private String cidade;
	@NotNull
	@ManyToOne
	@JoinColumn(name = "codigo_estado")
	private Estados estado;
	@NotNull
	private String cep;
	
	
	public String getEndereco() {
		return endereco != null ? endereco.toUpperCase() : endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getNumero() {
		return numero != null ? numero.toUpperCase() : numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getBairro() {
		return bairro != null ? bairro.toUpperCase() : bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCidade() {
		return cidade != null ? cidade.toUpperCase() : cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public Estados getEstado() {
		return estado;
	}
	public void setEstado(Estados estado) {
		this.estado = estado;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	

	
	

}
