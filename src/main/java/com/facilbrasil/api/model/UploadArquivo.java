package com.facilbrasil.api.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "upload_arquivo")
public class UploadArquivo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;	
	
	private String arquivo;	
	
	@ManyToOne
	@JoinColumn(name = "codigo_cliente")
	private Clientes cliente;	
	
	private Date dataImportacao;
	
	private Long quantidade;
	
	@Column(name = "saldo_a_cobrar")
	private BigDecimal saldoACobrar;
	
	
	public UploadArquivo() {
		super();
		this.setDataImportacao(new Date());
	}


	public Long getCodigo() {
		return codigo;
	}


	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}


	public String getArquivo() {
		return arquivo;
	}


	public void setArquivo(String arquivo) {
		this.arquivo = arquivo;
	}

	public Date getDataImportacao() {
		return dataImportacao;
	}


	public void setDataImportacao(Date dataImportacao) {
		this.dataImportacao = dataImportacao;
	}


	public Clientes getCliente() {
		return cliente;
	}


	public void setCliente(Clientes cliente) {
		this.cliente = cliente;
	}


	public BigDecimal getSaldoACobrar() {
		return saldoACobrar;
	}


	public void setSaldoACobrar(BigDecimal saldoACobrar) {
		this.saldoACobrar = saldoACobrar;
	}


	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}


	public Long getQuantidade() {
		return quantidade;
	}
	
	
}
