package com.facilbrasil.api.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Titulos.class)
public abstract class Titulos_ {

	public static volatile SingularAttribute<Titulos, Date> dataVencimentoDivida;
	public static volatile SingularAttribute<Titulos, Estados> estado;
	public static volatile SingularAttribute<Titulos, String> numero;
	public static volatile SingularAttribute<Titulos, String> agencia;
	public static volatile SingularAttribute<Titulos, String> cep;
	public static volatile SingularAttribute<Titulos, String> complemento;
	public static volatile SingularAttribute<Titulos, String> contratoConta;
	public static volatile SingularAttribute<Titulos, String> celular;
	public static volatile SingularAttribute<Titulos, Date> dataEnvioEntrada;
	public static volatile SingularAttribute<Titulos, String> classeLojaSetor;
	public static volatile SingularAttribute<Titulos, String> email;
	public static volatile SingularAttribute<Titulos, Long> codigo;
	public static volatile SingularAttribute<Titulos, String> telefone1;
	public static volatile SingularAttribute<Titulos, String> endereco;
	public static volatile SingularAttribute<Titulos, String> bairro;
	public static volatile SingularAttribute<Titulos, String> municipio;
	public static volatile SingularAttribute<Titulos, BigDecimal> saldoACobrar;
	public static volatile SingularAttribute<Titulos, Bancos> banco;
	public static volatile SingularAttribute<Titulos, String> nome;
	public static volatile SingularAttribute<Titulos, String> telefone2;
	public static volatile SingularAttribute<Titulos, String> descricao;
	public static volatile SingularAttribute<Titulos, Clientes> cliente;
	public static volatile SingularAttribute<Titulos, TipoDivida> tipoDivida;
	public static volatile SingularAttribute<Titulos, String> contaCorrente;
	public static volatile SingularAttribute<Titulos, String> cpfCnpj;
	public static volatile SingularAttribute<Titulos, String> numOuTituloCheque;
	public static volatile SingularAttribute<Titulos, BigDecimal> valorOriginal;
	public static volatile SingularAttribute<Titulos, String> referencia;

}

