package com.facilbrasil.api.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Mensagem.class)
public abstract class Mensagem_ {

	public static volatile SingularAttribute<Mensagem, String> assunto;
	public static volatile SingularAttribute<Mensagem, Long> codigo;
	public static volatile SingularAttribute<Mensagem, String> mensagem;
	public static volatile SingularAttribute<Mensagem, Long> codigoCliente;
	public static volatile SingularAttribute<Mensagem, Boolean> interno;
	public static volatile SingularAttribute<Mensagem, String> nome;
	public static volatile SingularAttribute<Mensagem, String> email;

}

