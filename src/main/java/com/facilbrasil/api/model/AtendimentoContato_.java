package com.facilbrasil.api.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AtendimentoContato.class)
public abstract class AtendimentoContato_ {

	public static volatile SingularAttribute<AtendimentoContato, Long> codigo;
	public static volatile SingularAttribute<AtendimentoContato, String> telefone;
	public static volatile SingularAttribute<AtendimentoContato, String> horario;
	public static volatile SingularAttribute<AtendimentoContato, Long> codigoCliente;
	public static volatile SingularAttribute<AtendimentoContato, String> nome;
	public static volatile SingularAttribute<AtendimentoContato, String> cpfCnpj;

}

