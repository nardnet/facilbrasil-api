package com.facilbrasil.api.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Arquivo.class)
public abstract class Arquivo_ {

	public static volatile SingularAttribute<Arquivo, Long> codigo;
	public static volatile SingularAttribute<Arquivo, BigDecimal> valorLiquido;
	public static volatile SingularAttribute<Arquivo, String> numeroInscricao;
	public static volatile SingularAttribute<Arquivo, String> pagador;
	public static volatile SingularAttribute<Arquivo, BigDecimal> valorTitulo;
	public static volatile SingularAttribute<Arquivo, String> arquivo;
	public static volatile SingularAttribute<Arquivo, BigDecimal> valorPago;
	public static volatile SingularAttribute<Arquivo, String> tipoInscricao;
	public static volatile SingularAttribute<Arquivo, String> error;
	public static volatile SingularAttribute<Arquivo, Date> dataImportacao;

}

