package com.facilbrasil.api.dto;

import java.util.ArrayList;
import java.util.List;
/**
 * Classe para testes de relatorio contrato
 */
public class ContratoDTOFactory {
	
	

   public static List<ContratoDTO> contrato() {		
		List<ContratoDTO> listContrato = new ArrayList<ContratoDTO>();
		ContratoDTO contrato = new ContratoDTO();
		contrato.setRzoSocial("Cobrarr LTDA");
		contrato.setCidade("Sete Lagoas");
		contrato.setUf("MG");
		contrato.setEndereco("Rua Ciprestes");
		contrato.setNumero("69");
		contrato.setBairro("Eldorado");
		contrato.setCep("32310-530");
		contrato.setCnpj("43.423.444/4444-44");
		contrato.setContaCorrente("10154-0");
		contrato.setAgencia("3211-5");
		contrato.setBanco("001 - BANCO DO BRASIL S/A");
		listContrato.add(contrato);
		return listContrato;
				
	}
	
	

}
