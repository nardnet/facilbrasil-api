package com.facilbrasil.api.utils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;

import com.facilbrasil.api.model.TbConfig;
@Service
public class PropertiesUtils {
	
	@PersistenceContext
	private EntityManager manager;
	
	public String getLoadconfigBanco(String key) {		
	

		Query query = manager.createQuery("select a from TbConfig a where chave = ?1");
		query.setParameter(1, key);

		TbConfig conf = null;
		try {
			conf = (TbConfig) query.getSingleResult();
			return conf.getValor();
		} catch (Exception e) {
		System.out.println("Erro ao obter valor da chave: " + key + "," + e); 
		}

		return null;
	}

}
