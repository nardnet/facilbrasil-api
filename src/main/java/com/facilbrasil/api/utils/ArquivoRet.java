package com.facilbrasil.api.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import javax.swing.text.MaskFormatter;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.facilbrasil.api.mail.Mailer;
import com.facilbrasil.api.model.Arquivo;
import com.facilbrasil.api.model.Clientes;
import com.facilbrasil.api.model.UploadArquivo;
import com.facilbrasil.api.repository.ArquivoRepository;
import com.facilbrasil.api.repository.UploadArquivoRepository;

@Service
public class ArquivoRet {
	final String dir_boletos = "DIRBOLETOS";
	final String dir_boletos_sucesso = "DIRBOLETOSSUCCESS";
	final String dir_boletos_erro = "DIRBOLETOSERROR";
	final String dir_upload_arquivos = "DIRUPLOAD";
	
	final String coordenador = "COORDENADOR";
	final String analista = "ANALISTA";
	final String uploadArquivo = "mail/uploadArquivo";
	final String remetente = "REMETENTE";	

	
	@Autowired
	private Mailer mail;
	
	@Autowired
	private PropertiesUtils prop;
	
	@Autowired
	private ArquivoRepository arquivoRepository;
	
	@Autowired
	private UploadArquivoRepository uploadArquivoRepository;
	
	public void trasferArquivos(Long quantidade, BigDecimal saldoACobrar, Clientes cliente, MultipartFile file) throws IOException {
	    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Date hoje = Calendar.getInstance().getTime();
		String dataAtual = df.format(hoje);
		String extensaoDoArquivo = FilenameUtils.getExtension(file.getOriginalFilename());
		String nomeArquivo =  file.getName() + cliente.getNome() + dataAtual.replaceAll("/", "") + "."+extensaoDoArquivo;
		File convFile = new File(prop.getLoadconfigBanco(dir_upload_arquivos)+"/"+ nomeArquivo);
		file.transferTo(convFile);
		   
		UploadArquivo arquivo = new UploadArquivo();
	
		arquivo.setArquivo(file.getName());
		arquivo.setCliente(cliente);
	    arquivo.setQuantidade(quantidade);
	    arquivo.setSaldoACobrar(saldoACobrar);
		uploadArquivoRepository.save(arquivo);
		
 	    Map<String, Object> variaveis = new HashMap<>();
			 		variaveis.put("cliente", cliente);	
			 		variaveis.put("arquivo", nomeArquivo);	
		
		mail.enviarEmail(prop.getLoadconfigBanco(remetente), 
 				Arrays.asList(prop.getLoadconfigBanco(analista),prop.getLoadconfigBanco(coordenador)), 
 				"Envio de títulos via upload! Foram enviados títulos pelo cliente "+ cliente.getNome(), uploadArquivo, variaveis);
 		System.out.println("Terminado o envio de e-mail...");
		
	}

	public void importarArquivos() throws IOException {
		Scanner scanner;
		File file = new File(prop.getLoadconfigBanco(dir_boletos));
		File afile[] = file.listFiles();
		int i = 0;
       
		for (int j = afile.length; i < j; i++) {
			File arquivo = afile[i];
			ArrayList<String> lista = new ArrayList<String>();
			
			try {
				if (arquivo != null && arquivo.exists()) {

					if (arquivo.getName().endsWith(".ret")) {
						String extensaoDoArquivo = getFileExtension(arquivo.getName());
						System.out.println(extensaoDoArquivo);
						System.out.println(arquivo.getName());

						scanner = new Scanner(new FileReader(arquivo));

						while (scanner.hasNext()) {
							lista.add(scanner.nextLine());
						}
						
					
						  Arquivo arq = new Arquivo(); 
					
						  
							for (String line : lista) {
								  arq.setArquivo(arquivo.getName());
								  String segmento = line.substring(13, 14);
							
								if (segmento.equals("T")) {
							    	
									String tipoInscricao = line.substring(132, 133);
									String numeroInscricao = line.substring(133, 148);
									Long valorTitulo = new Long(line.substring(81, 96));
									String pagador = line.substring(148, 188);	
									
									numeroInscricao = formatCnpj(numeroInscricao, tipoInscricao);
									
									
									arq.setTipoInscricao(tipoInscricao);
									arq.setNumeroInscricao(numeroInscricao);
									arq.setValorTitulo(BigDecimal.valueOf(valorTitulo));
									arq.setPagador(pagador);
									
									System.out.println("tipoInscricao =>" + tipoInscricao);
									System.out.println("numeroInscricao =>" + numeroInscricao);
									System.out.println("valorNominal =>" + valorTitulo);
									System.out.println("pagador =>" + pagador);
									
									continue;
							    }

								
							    if (segmento.equals("U")) {
							    	
							   
							    	Long valorPago = new Long(line.substring(77, 92));
							    	Long valorLiquido = new Long(line.substring(92, 107));
																	
									arq.setValorPago(valorPago != null ? BigDecimal.valueOf(valorPago) : BigDecimal.ZERO);
									arq.setValorLiquido(valorLiquido != null ?  BigDecimal.valueOf(valorLiquido): BigDecimal.ZERO);

									System.out.println("valorPago =>" + valorPago);
									System.out.println("valorLiquido =>" + valorLiquido);
									
									  if (arq.getTipoInscricao() != null)
											arquivoRepository.save(arq);
									  
									  arq = new Arquivo(); 
							    } 
					    }

					} else {
						// Diretorio de destino error
						File diretorioDestino = new File(prop.getLoadconfigBanco(dir_boletos_erro), arquivo.getName());
						copiarArquivos(arquivo, diretorioDestino);
			
					}
				}			
					// Diretorio de destino success
					File diretorioDestino = new File(prop.getLoadconfigBanco(dir_boletos_sucesso), arquivo.getName());
					copiarArquivos(arquivo, diretorioDestino);
		

			} 
		    catch (Exception e) { 
				// Diretorio de destino error
				File diretorioDestino = new File(prop.getLoadconfigBanco(dir_boletos_erro), arquivo.getName());
				copiarArquivos(arquivo, diretorioDestino);
				e.printStackTrace();
				return;
			}


		}
	}

	public String getFileExtension(String filename) {
		if (filename.contains("."))
			return filename.substring(filename.lastIndexOf(".") + 1);
		else
			return "";
	}

	private String formatCnpj(String cnpjCpf, String inscricao) throws ParseException {

		try {
			if (inscricao.equals("2")) {
				MaskFormatter mask = new MaskFormatter("###.###.###/####-##");
				mask.setValueContainsLiteralCharacters(false);
				return mask.valueToString(cnpjCpf);
			} else if (inscricao.equals("1")) {
				MaskFormatter mask = new MaskFormatter("##.###.###/##");
				mask.setValueContainsLiteralCharacters(false);
				return mask.valueToString(cnpjCpf);
			}

		} catch (ParseException ex) {
			return cnpjCpf;
		}
		return cnpjCpf;
	}
	
	public void deletarArquivos() {
		System.gc();
		File file = new File(prop.getLoadconfigBanco(dir_boletos));
		File afile[] = file.listFiles();
		if (file.isDirectory()) {
			for (File toDelete : afile) {
			 toDelete.delete();
			}
		}
	}

	@SuppressWarnings("resource")
	public static void copiarArquivos(File source, File destination) throws IOException {
		if (destination.exists())
			destination.delete();
		FileChannel sourceChannel = null;
		FileChannel destinationChannel = null;
		try {
			sourceChannel = new FileInputStream(source).getChannel();
			destinationChannel = new FileOutputStream(destination).getChannel();
			sourceChannel.transferTo(0, sourceChannel.size(), destinationChannel);
		} finally {

			if (sourceChannel != null && sourceChannel.isOpen())
				sourceChannel.close();
			if (destinationChannel != null && destinationChannel.isOpen())
				destinationChannel.close();
		}
	}

}
