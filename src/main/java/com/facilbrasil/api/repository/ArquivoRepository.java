package com.facilbrasil.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.facilbrasil.api.model.Arquivo;
import com.facilbrasil.api.repository.arquivos.ArquivoRepositoryQuery;

public interface  ArquivoRepository extends JpaRepository<Arquivo, Long> , ArquivoRepositoryQuery {

	public List<Arquivo> findArquivos(String numeroInscricao);
}
