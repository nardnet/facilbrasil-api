package com.facilbrasil.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.facilbrasil.api.model.Bancos;

public interface BancosRepository extends JpaRepository<Bancos, Long> {

}
