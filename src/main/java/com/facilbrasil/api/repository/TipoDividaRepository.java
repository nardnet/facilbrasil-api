package com.facilbrasil.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.facilbrasil.api.model.TipoDivida;


public interface TipoDividaRepository extends JpaRepository<TipoDivida, Long> {

}
