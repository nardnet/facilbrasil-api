package com.facilbrasil.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.facilbrasil.api.model.Estados;

public interface EstadosRepository extends JpaRepository<Estados, Long> {

}
