package com.facilbrasil.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.facilbrasil.api.model.Clientes;
import com.facilbrasil.api.repository.clientes.ClientesRepositoryQuery;
import com.facilbrasil.api.repository.filter.ClienteFilter;



public interface ClientesRepository extends JpaRepository<Clientes, Long>, ClientesRepositoryQuery {
	
	public Page<Clientes> filtrar(ClienteFilter clienteFilter, Pageable pageable);

}
