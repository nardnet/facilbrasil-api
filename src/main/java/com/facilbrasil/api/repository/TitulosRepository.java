package com.facilbrasil.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.facilbrasil.api.model.Titulos;
import com.facilbrasil.api.repository.titulos.TitulosRepositoryQuery;

public interface TitulosRepository extends JpaRepository<Titulos, Long>, TitulosRepositoryQuery {
	
	public Page<Titulos> findByNomeContaining(String nome, Pageable pageable);
}
