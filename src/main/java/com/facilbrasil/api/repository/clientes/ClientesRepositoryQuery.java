package com.facilbrasil.api.repository.clientes;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.facilbrasil.api.model.Clientes;
import com.facilbrasil.api.repository.filter.ClienteFilter;
import com.facilbrasil.api.repository.projection.ResumoCliente;


public interface ClientesRepositoryQuery {
	
	public Clientes getClienteByCnpj(String cnpj);
	public Page<Clientes> filtrar(ClienteFilter clienteFilter, Pageable pageable);
	public Page<ResumoCliente> resumir(ClienteFilter clienteFilter, Pageable pageable);
}
