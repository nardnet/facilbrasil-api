package com.facilbrasil.api.repository.clientes;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import com.facilbrasil.api.model.Bancos_;
import com.facilbrasil.api.model.Clientes;
import com.facilbrasil.api.model.Clientes_;
import com.facilbrasil.api.model.Endereco_;
import com.facilbrasil.api.repository.filter.ClienteFilter;
import com.facilbrasil.api.repository.projection.ResumoCliente;

public class ClientesRepositoryImpl implements ClientesRepositoryQuery{
	
	@PersistenceContext
	private EntityManager manager;

	@Override
	public Page<Clientes> filtrar(ClienteFilter clienteFilter, Pageable pageable) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Clientes> criteria = builder.createQuery(Clientes.class);
		Root<Clientes> root = criteria.from(Clientes.class);
		
		Predicate[] predicates = criarRestricoes(clienteFilter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<Clientes> query = manager.createQuery(criteria);
        adicionarRestricoesDePaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(clienteFilter));
		
	}

	@Override
	public Page<ResumoCliente> resumir(ClienteFilter clienteFilter, Pageable pageable) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<ResumoCliente> criteria = builder.createQuery(ResumoCliente.class);
		Root<Clientes> root = criteria.from(Clientes.class);
		
		
		criteria.select(builder.construct(ResumoCliente.class
				
				, root.get(Clientes_.codigo), root.get(Clientes_.ativo), root.get(Clientes_.nome)
				, root.get(Clientes_.email),  root.get(Clientes_.cpf),  
				  root.get(Clientes_.rzoSocial)
				, root.get(Clientes_.cnpj),   root.get(Clientes_.inscEstadual)
				, root.get(Clientes_.endereco).get(Endereco_.endereco), root.get(Clientes_.endereco).get(Endereco_.numero)
				, root.get(Clientes_.endereco).get(Endereco_.bairro),root.get(Clientes_.endereco).get(Endereco_.cidade), 
				 root.get(Clientes_.telefone)				
				, root.get(Clientes_.whatsapp),  root.get(Clientes_.strAtividade)
				, root.get(Clientes_.princConcorrentes),  root.get(Clientes_.indInadimplencia)
				, root.get(Clientes_. empGroup),  root.get(Clientes_.contCobranca)
				, root.get(Clientes_.cargo),  root.get(Clientes_.repNome)
				, root.get(Clientes_.repCpf),  root.get(Clientes_.repEmail)
				, root.get(Clientes_.observacoes), root.get(Clientes_.banco).get(Bancos_.banco)
				, root.get(Clientes_.agencia), root.get(Clientes_.contaCorrente)
				, root.get(Clientes_.operacao), root.get(Clientes_.dtCadastro)
				
			));
		
		
		Predicate[] predicates = criarRestricoes(clienteFilter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<ResumoCliente> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(clienteFilter));
	}
	
	private Predicate[] criarRestricoes(ClienteFilter clienteFilter, CriteriaBuilder builder,
			Root<Clientes> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		if (!StringUtils.isEmpty(clienteFilter.getNome())) {
			predicates.add(builder.like(
					builder.lower(root.get(Clientes_.nome)), "%" + clienteFilter.getNome().toLowerCase() + "%"));
		}
		
		if (!StringUtils.isEmpty(clienteFilter.getCpf())) {
			predicates.add(builder.like(
					builder.lower(root.get(Clientes_.cpf)), "%" + clienteFilter.getCpf().toLowerCase() + "%"));
		}
		
		if (!StringUtils.isEmpty(clienteFilter.getTelefone())) {
			predicates.add(builder.like(
					builder.lower(root.get(Clientes_.telefone)), "%" + clienteFilter.getTelefone().toLowerCase() + "%"));
		}
		 
		if (!StringUtils.isEmpty(clienteFilter.getInativo())) {
			predicates.add(
					builder.equal(root.get(Clientes_.ativo), clienteFilter.getInativo()));
		}else {
			predicates.add(
					builder.equal(root.get(Clientes_.ativo), true));
		}
		
		
		if (clienteFilter.getDataCadastroDe() != null) {
			predicates.add(
					builder.greaterThanOrEqualTo(root.get(Clientes_.dtCadastro),  clienteFilter.getDataCadastroDe()));
		}
		
		if (clienteFilter.getDataCadastroAte()!= null) {
			predicates.add(
					builder.lessThanOrEqualTo(root.get(Clientes_.dtCadastro), clienteFilter.getDataCadastroAte()));
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(ClienteFilter clienteFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Clientes> root = criteria.from(Clientes.class);
		
		Predicate[] predicates = criarRestricoes(clienteFilter, builder, root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

	@Override
	public Clientes getClienteByCnpj(String cnpj) {
		Query query = manager.createQuery("select max(a) from Clientes a where cnpj = ?1");
		query.setParameter(1, cnpj);

		Clientes cliente = null;
		try {
			cliente = (Clientes) query.getSingleResult();
			return cliente;
		} catch (Exception e) {	
		}
		return cliente;
	}

}
