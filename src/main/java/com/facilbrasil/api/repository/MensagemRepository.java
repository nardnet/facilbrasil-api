package com.facilbrasil.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.facilbrasil.api.model.Mensagem;
import com.facilbrasil.api.repository.mensagem.MensagemRepositoryQuery;

public interface MensagemRepository extends JpaRepository<Mensagem, Long>, MensagemRepositoryQuery {
	
	public List<Mensagem> findMensagens(Long codigo);
}
