package com.facilbrasil.api.repository;


import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.facilbrasil.api.model.Usuario;
import com.facilbrasil.api.repository.usuario.UsuarioRepositoryQuery;

public interface UsuarioRepository extends JpaRepository<Usuario, Long>, UsuarioRepositoryQuery {

	public Optional<Usuario> findByUsuario(String usuario);

	public List<Usuario> findUsuarioSemCliente();

}
