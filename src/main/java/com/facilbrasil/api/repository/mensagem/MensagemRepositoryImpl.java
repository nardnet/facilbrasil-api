package com.facilbrasil.api.repository.mensagem;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.facilbrasil.api.model.Mensagem;

public class MensagemRepositoryImpl implements MensagemRepositoryQuery {
	@PersistenceContext
	private EntityManager manager;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Mensagem> findMensagens(Long codigo) {
		List<Mensagem> lista = new ArrayList<Mensagem>();
		
		Query query = manager.createQuery("select a from Mensagem a where codigoCliente = ?1");	
		query.setParameter(1, codigo);
		try {
			lista = (List<Mensagem>) query.getResultList();
			return lista;
		} catch (Exception e) {	
		}
		return lista;
	}

}
