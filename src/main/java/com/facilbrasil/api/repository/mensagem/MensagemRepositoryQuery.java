package com.facilbrasil.api.repository.mensagem;

import java.util.List;

import com.facilbrasil.api.model.Mensagem;

public interface MensagemRepositoryQuery {
	public List<Mensagem> findMensagens(Long codigo);
}
