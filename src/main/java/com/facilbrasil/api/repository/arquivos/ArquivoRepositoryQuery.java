package com.facilbrasil.api.repository.arquivos;

import java.math.BigDecimal;
import java.util.List;

import com.facilbrasil.api.model.Arquivo;

public interface ArquivoRepositoryQuery {
	public List<Arquivo> findArquivos(String numeroInscricao);
	
	public BigDecimal sumValorPago(String numeroInscricao);
}
