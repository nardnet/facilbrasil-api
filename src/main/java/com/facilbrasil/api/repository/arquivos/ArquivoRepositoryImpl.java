package com.facilbrasil.api.repository.arquivos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.facilbrasil.api.model.Arquivo;

public class ArquivoRepositoryImpl implements ArquivoRepositoryQuery {
	@PersistenceContext
	private EntityManager manager;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Arquivo> findArquivos(String numeroInscricao) {
		List<Arquivo> lista = new ArrayList<Arquivo>();
		
		Query query = manager.createQuery("select a from Arquivo a where numeroInscricao = ?1");	
		query.setParameter(1, numeroInscricao);
		try {
			lista = (List<Arquivo>) query.getResultList();
			return lista;
		} catch (Exception e) {	
		}
		return lista;
	}

	@Override
	public BigDecimal sumValorPago(String numeroInscricao) {
		   BigDecimal valor = BigDecimal.ZERO;
			
			Query query = manager.createQuery("select sum(a.valorPago) from Arquivo a where numeroInscricao = ?1");	
			query.setParameter(1, numeroInscricao);
			try {
				valor = (BigDecimal) query.getSingleResult();
				return valor != null ? valor : BigDecimal.ZERO;
			} catch (Exception e) {	
			}
			return valor;
	}

}
