package com.facilbrasil.api.repository.titulos;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.facilbrasil.api.model.Clientes;
import com.facilbrasil.api.model.Titulos;
import com.facilbrasil.api.repository.filter.TituloFilter;
import com.facilbrasil.api.repository.projection.ResumoTitulos;

public interface TitulosRepositoryQuery {
	
	public Page<Titulos> filtrar(TituloFilter titulosFilter, Pageable pageable);
	public Page<ResumoTitulos> resumir(TituloFilter titulosFilter, Pageable pageable);
	public List<Titulos> findByTitulosHoje(Date data);
	public BigDecimal sumValorTitulos(Clientes cliente);
}
