package com.facilbrasil.api.repository.titulos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import com.facilbrasil.api.model.Bancos_;
import com.facilbrasil.api.model.Clientes;
import com.facilbrasil.api.model.Clientes_;
import com.facilbrasil.api.model.Estados_;
import com.facilbrasil.api.model.TipoDivida_;
import com.facilbrasil.api.model.Titulos;
import com.facilbrasil.api.model.Titulos_;
import com.facilbrasil.api.repository.ClientesRepository;
import com.facilbrasil.api.repository.filter.TituloFilter;
import com.facilbrasil.api.repository.projection.ResumoTitulos;

public class TitulosRepositoryImpl implements TitulosRepositoryQuery{
	
	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
	ClientesRepository clienteRepository;
	

	@Override
	public Page<Titulos> filtrar(TituloFilter titulosFilter, Pageable pageable) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Titulos> criteria = builder.createQuery(Titulos.class);
		Root<Titulos> root = criteria.from(Titulos.class);
		
		Predicate[] predicates = criarRestricoes(titulosFilter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<Titulos> query = manager.createQuery(criteria);
        adicionarRestricoesDePaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(titulosFilter));
	}
	
	@Override
	public Page<ResumoTitulos> resumir(TituloFilter titulosFilter, Pageable pageable) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<ResumoTitulos> criteria = builder.createQuery(ResumoTitulos.class);
		Root<Titulos> root = criteria.from(Titulos.class);
		
		criteria.select(builder.construct(ResumoTitulos.class
				, root.get(Titulos_.codigo), root.get(Titulos_.cliente).get(Clientes_.nome)
				, root.get(Titulos_.nome), root.get(Titulos_.cpfCnpj)
				, root.get(Titulos_.contratoConta), root.get(Titulos_.tipoDivida).get(TipoDivida_.tipo)
				, root.get(Titulos_.numOuTituloCheque)
				, root.get(Titulos_.banco).get(Bancos_.banco), root.get(Titulos_.agencia)
				, root.get(Titulos_.contaCorrente)
				, root.get(Titulos_.dataEnvioEntrada), root.get(Titulos_.dataVencimentoDivida)
				, root.get(Titulos_.valorOriginal), root.get(Titulos_.saldoACobrar)
				, root.get(Titulos_.endereco), root.get(Titulos_.numero)
				, root.get(Titulos_.complemento), root.get(Titulos_.bairro)
				, root.get(Titulos_.municipio), root.get(Titulos_.estado).get(Estados_.nome)
				, root.get(Titulos_.cep), root.get(Titulos_.telefone1)
				, root.get(Titulos_.telefone2), root.get(Titulos_.celular)
				, root.get(Titulos_.referencia)
				, root.get(Titulos_.classeLojaSetor), root.get(Titulos_.email)
				, root.get(Titulos_.descricao)));
		
		Predicate[] predicates = criarRestricoes(titulosFilter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<ResumoTitulos> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(titulosFilter));
	
	}
	
	private Predicate[] criarRestricoes(TituloFilter titulosFilter, CriteriaBuilder builder,
			Root<Titulos> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		if (!StringUtils.isEmpty(titulosFilter.getContratoConta())) {
			predicates.add(builder.like(
					builder.lower(root.get(Titulos_.contratoConta)), "%" + titulosFilter.getContratoConta().toLowerCase() + "%"));
		}
		
		if (titulosFilter.getDataVencimentoDividaDe() != null) {
			predicates.add(
					builder.greaterThanOrEqualTo(root.get(Titulos_.dataVencimentoDivida), titulosFilter.getDataVencimentoDividaDe()));
		}
		
		if (titulosFilter.getDataVencimentoDividaAte() != null) {
			predicates.add(
					builder.lessThanOrEqualTo(root.get(Titulos_.dataVencimentoDivida), titulosFilter.getDataVencimentoDividaAte()));
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(TituloFilter tituloFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Titulos> root = criteria.from(Titulos.class);
		
		Predicate[] predicates = criarRestricoes(tituloFilter, builder, root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Titulos> findByTitulosHoje(Date data) {
		List<Titulos> lista = new ArrayList<Titulos>();
		
	
		Query query = manager.createQuery("select a from Titulos a where DATE(dataEnvioEntrada) = ?1");
		query.setParameter(1, data);

		
		try {
			lista = (List<Titulos>) query.getResultList();
			return lista;
		} catch (Exception e) {	
		}
		return lista;
	}

	@Override
	public BigDecimal sumValorTitulos(Clientes cliente) {
	    BigDecimal valor = BigDecimal.ZERO;
	
		Query query = manager.createNativeQuery("select sum(T.saldoACobrar)  from ( "
				+ " select sum(a.saldo_a_cobrar ) as saldoACobrar from facilbrasilapi.titulos a "
				+ " where a.codigo_cliente = :1 union "
		        + " select sum(u.saldo_a_cobrar ) as saldoACobrar from facilbrasilapi.upload_arquivo u "
		        + " where u.codigo_cliente = :2  ) T ");	
		query.setParameter("1", cliente.getCodigo());
		query.setParameter("2", cliente.getCodigo());
		try {
			valor = (BigDecimal) query.getSingleResult();
			return valor != null ? valor : BigDecimal.ZERO;
		} catch (Exception e) {	
		}
		return valor;
	}

	

}
