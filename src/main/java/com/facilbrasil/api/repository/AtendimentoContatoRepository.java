package com.facilbrasil.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.facilbrasil.api.model.AtendimentoContato;

public interface AtendimentoContatoRepository extends JpaRepository<AtendimentoContato, Long> {

}
