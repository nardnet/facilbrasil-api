package com.facilbrasil.api.repository.filter;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class TituloFilter {
	
	private String contratoConta;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date  dataVencimentoDividaDe;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date  dataVencimentoDividaAte;
	
	public String getContratoConta() {
		return contratoConta;
	}
	public void setContratoConta(String contratoConta) {
		this.contratoConta = contratoConta;
	}
	public Date getDataVencimentoDividaDe() {
		return dataVencimentoDividaDe;
	}
	public void setDataVencimentoDividaDe(Date dataVencimentoDividaDe) {
		this.dataVencimentoDividaDe = dataVencimentoDividaDe;
	}
	public Date getDataVencimentoDividaAte() {
		return dataVencimentoDividaAte;
	}
	public void setDataVencimentoDividaAte(Date dataVencimentoDividaAte) {
		this.dataVencimentoDividaAte = dataVencimentoDividaAte;
	}
	
	

}
