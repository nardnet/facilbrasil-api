package com.facilbrasil.api.repository.filter;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class ClienteFilter {
	
	private String nome;
	private String telefone;
	private String cpf;
	private Boolean inativo;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date  dataCadastroDe;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date  dataCadastroAte;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Date getDataCadastroDe() {
		return dataCadastroDe;
	}

	public void setDataCadastroDe(Date dataCadastroDe) {
		this.dataCadastroDe = dataCadastroDe;
	}

	public Date getDataCadastroAte() {
		return dataCadastroAte;
	}

	public void setDataCadastroAte(Date dataCadastroAte) {
		this.dataCadastroAte = dataCadastroAte;
	}

	public Boolean getInativo() {
		return inativo;
	}

	public void setInativo(Boolean inativo) {
		this.inativo = !inativo;
	}


	
	

	
	
	

}
