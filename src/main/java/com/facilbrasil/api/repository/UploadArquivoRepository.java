package com.facilbrasil.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.facilbrasil.api.model.UploadArquivo;

public interface UploadArquivoRepository extends JpaRepository<UploadArquivo, Long> {

}
