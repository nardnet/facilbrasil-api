package com.facilbrasil.api.repository.projection;

import java.math.BigDecimal;
import java.util.Date;

public class ResumoTitulos {
	
	private Long codigo;
	private String cliente;	
	private String nome;
	private String cpfCnpj;	
	private String contratoConta;
	private String tipoDivida;	
	private String numOuTituloCheque;
	private String banco;
	private String agencia;
	private String contaCorrente;
	private Date dataEnvioEntrada;
	private Date dataVencimentoDivida;
	private BigDecimal valorOriginal;	
	private BigDecimal saldoACobrar;
	private String endereco;
	private String numero;
	private String complemento;
	private String bairro;
	private String municipio;
	private String estado;
	private String cep;
	private String telefone1;
	private String telefone2;
	private String celular;
	private String referencia;
	private String classeLojaSetor;
	private String email;
	private String descricao;
	
	public ResumoTitulos(Long codigo, String cliente, String nome, String cpfCnpj, String contratoConta,
			String tipoDivida, String numOuTituloCheque, String numeroCheque, String banco, String agencia,
			String contaCorrente, Date dataEnvioEntrada, Date dataVencimentoDivida, BigDecimal valorOriginal,
			BigDecimal saldoACobrar, String endereco, String numero, String complemento, String bairro,
			String municipio, String estado, String cep, String telefone1, String telefone2, String celular,
			String referencia, String classeLojaSetor, String email, String descricao) {
		super();
		this.codigo = codigo;
		this.cliente = cliente;
		this.nome = nome;
		this.cpfCnpj = cpfCnpj;
		this.contratoConta = contratoConta;
		this.tipoDivida = tipoDivida;
		this.numOuTituloCheque = numOuTituloCheque;		
		this.banco = banco;
		this.agencia = agencia;
		this.contaCorrente = contaCorrente;
		this.dataEnvioEntrada = dataEnvioEntrada;
		this.dataVencimentoDivida = dataVencimentoDivida;
		this.valorOriginal = valorOriginal;
		this.saldoACobrar = saldoACobrar;
		this.endereco = endereco;
		this.numero = numero;
		this.complemento = complemento;
		this.bairro = bairro;
		this.municipio = municipio;
		this.estado = estado;
		this.cep = cep;
		this.telefone1 = telefone1;
		this.telefone2 = telefone2;
		this.celular = celular;
		this.referencia = referencia;
		this.classeLojaSetor = classeLojaSetor;
		this.email = email;
		this.descricao = descricao;
	}
	
	
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpfCnpj() {
		return cpfCnpj;
	}
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	public String getContratoConta() {
		return contratoConta;
	}
	public void setContratoConta(String contratoConta) {
		this.contratoConta = contratoConta;
	}
	public String getTipoDivida() {
		return tipoDivida;
	}
	public void setTipoDivida(String tipoDivida) {
		this.tipoDivida = tipoDivida;
	}
	public String getNumOuTituloCheque() {
		return numOuTituloCheque;
	}
	public void setNumOuTituloCheque(String numOuTituloCheque) {
		this.numOuTituloCheque = numOuTituloCheque;
	}
	public String getBanco() {
		return banco;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
	public String getAgencia() {
		return agencia;
	}
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	public String getContaCorrente() {
		return contaCorrente;
	}
	public void setContaCorrente(String contaCorrente) {
		this.contaCorrente = contaCorrente;
	}
	public Date getDataEnvioEntrada() {
		return dataEnvioEntrada;
	}
	public void setDataEnvioEntrada(Date dataEnvioEntrada) {
		this.dataEnvioEntrada = dataEnvioEntrada;
	}
	public Date getDataVencimentoDivida() {
		return dataVencimentoDivida;
	}
	public void setDataVencimentoDivida(Date dataVencimentoDivida) {
		this.dataVencimentoDivida = dataVencimentoDivida;
	}
	public BigDecimal getValorOriginal() {
		return valorOriginal;
	}
	public void setValorOriginal(BigDecimal valorOriginal) {
		this.valorOriginal = valorOriginal;
	}
	public BigDecimal getSaldoACobrar() {
		return saldoACobrar;
	}
	public void setSaldoACobrar(BigDecimal saldoACobrar) {
		this.saldoACobrar = saldoACobrar;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getTelefone1() {
		return telefone1;
	}
	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}
	public String getTelefone2() {
		return telefone2;
	}
	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getClasseLojaSetor() {
		return classeLojaSetor;
	}
	public void setClasseLojaSetor(String classeLojaSetor) {
		this.classeLojaSetor = classeLojaSetor;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
}
