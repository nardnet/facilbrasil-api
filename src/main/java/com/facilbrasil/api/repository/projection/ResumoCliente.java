package com.facilbrasil.api.repository.projection;

import java.util.Date;


public class ResumoCliente {
	

	private Long codigo;	
	private Boolean ativo;
	private String nome;
	private String email;	
	private String cpf;
	private String rzoSocial;
	private String cnpj;
	private String inscEstadual;
	private String endereco;
	private String numero;
	private String bairro;	
	private String cidade;
	private String telefone;	
	private String whatsapp;
	private String strAtividade;	
	private String princConcorrentes;
	private String indInadimplencia;
	private String empGroup;
	private String contCobranca;
	private String cargo;
	private String repNome;
	private String repCpf;	
	private String repEmail;
	private String observacoes;
	private String banco;
	private String agencia;	
	private String contaCorrente;	
	private String operacao;
	private Date dtCadastro;
	
	public ResumoCliente(Long codigo,Boolean ativo, String nome, String email, String cpf, String rzoSocial,
			String cnpj, String inscEstadual, String endereco, String numero, String bairro, String cidade,
			String telefone, String whatsapp, String strAtividade, String princConcorrentes, String indInadimplencia,
			String empGroup, String contCobranca, String cargo, String repNome, String repCpf, String repEmail,
			String observacoes, String banco, String agencia, String contaCorrente, String operacao, Date dtCadastro) {
		super();
		this.codigo = codigo;
		this.ativo = ativo;
		this.nome = nome;
		this.email = email;		
		this.cpf = cpf;
		this.rzoSocial = rzoSocial;
		this.cnpj = cnpj;
		this.inscEstadual = inscEstadual;
		this.endereco = endereco;
		this.numero = numero;
		this.bairro = bairro;
		this.cidade = cidade;
		this.telefone = telefone;
		this.whatsapp = whatsapp;
		this.strAtividade = strAtividade;
		this.princConcorrentes = princConcorrentes;
		this.indInadimplencia = indInadimplencia;
		this.empGroup = empGroup;
		this.contCobranca = contCobranca;
		this.cargo = cargo;
		this.repNome = repNome;
		this.repCpf = repCpf;
		this.repEmail = repEmail;
		this.observacoes = observacoes;
		this.banco = banco;
		this.agencia = agencia;
		this.contaCorrente = contaCorrente;
		this.operacao = operacao;
		this.dtCadastro = dtCadastro;
	}

	
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getRzoSocial() {
		return rzoSocial;
	}
	public void setRzoSocial(String rzoSocial) {
		this.rzoSocial = rzoSocial;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getInscEstadual() {
		return inscEstadual;
	}
	public void setInscEstadual(String inscEstadual) {
		this.inscEstadual = inscEstadual;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getWhatsapp() {
		return whatsapp;
	}
	public void setWhatsapp(String whatsapp) {
		this.whatsapp = whatsapp;
	}
	public String getStrAtividade() {
		return strAtividade;
	}
	public void setStrAtividade(String strAtividade) {
		this.strAtividade = strAtividade;
	}
	public String getPrincConcorrentes() {
		return princConcorrentes;
	}
	public void setPrincConcorrentes(String princConcorrentes) {
		this.princConcorrentes = princConcorrentes;
	}
	public String getIndInadimplencia() {
		return indInadimplencia;
	}
	public void setIndInadimplencia(String indInadimplencia) {
		this.indInadimplencia = indInadimplencia;
	}
	public String getEmpGroup() {
		return empGroup;
	}
	public void setEmpGroup(String empGroup) {
		this.empGroup = empGroup;
	}
	public String getContCobranca() {
		return contCobranca;
	}
	public void setContCobranca(String contCobranca) {
		this.contCobranca = contCobranca;
	}
	public String getCargo() {
		return cargo;
	}
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	public String getRepNome() {
		return repNome;
	}
	public void setRepNome(String repNome) {
		this.repNome = repNome;
	}
	public String getRepCpf() {
		return repCpf;
	}
	public void setRepCpf(String repCpf) {
		this.repCpf = repCpf;
	}
	public String getRepEmail() {
		return repEmail;
	}
	public void setRepEmail(String repEmail) {
		this.repEmail = repEmail;
	}
	public String getObservacoes() {
		return observacoes;
	}
	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}
	public String getBanco() {
		return banco;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
	public String getAgencia() {
		return agencia;
	}
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	public String getContaCorrente() {
		return contaCorrente;
	}
	public void setContaCorrente(String contaCorrente) {
		this.contaCorrente = contaCorrente;
	}
	public String getOperacao() {
		return operacao;
	}
	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}
	public Date getDtCadastro() {
		return dtCadastro;
	}
	public void setDtCadastro(Date dtCadastro) {
		this.dtCadastro = dtCadastro;
	}


	public Boolean getAtivo() {
		return ativo;
	}


	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	

     

}
