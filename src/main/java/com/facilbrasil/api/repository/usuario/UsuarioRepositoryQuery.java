package com.facilbrasil.api.repository.usuario;

import java.util.List;

import com.facilbrasil.api.model.Usuario;

public interface UsuarioRepositoryQuery {
	
    public Usuario findUsuarioPorCliente(Long cliente);

    public List<Usuario> findUsuarioSemCliente();
}
