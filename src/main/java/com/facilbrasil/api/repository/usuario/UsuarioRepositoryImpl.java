package com.facilbrasil.api.repository.usuario;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.facilbrasil.api.model.Clientes;
import com.facilbrasil.api.model.Usuario;

public class UsuarioRepositoryImpl implements UsuarioRepositoryQuery {
	@PersistenceContext
	private EntityManager manager;

	@Override
	public Usuario findUsuarioPorCliente(Long cliente) {
		Clientes clientes = new Clientes();
		clientes.setCodigo(cliente);
		Query query = manager.createQuery("select a from Usuario a where cliente = ?1");
		query.setParameter(1, clientes);

		Usuario usuario = new Usuario();
		try {
			usuario = (Usuario) query.getSingleResult();
			return usuario;
		} catch (Exception e) {	
		}
		return usuario;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Usuario> findUsuarioSemCliente() {
		List<Usuario> lista = new ArrayList<Usuario>();
		
		Query query = manager.createQuery("select a from Usuario a where cliente is null");	
		
		try {
			lista = (List<Usuario>) query.getResultList();
			return lista;
		} catch (Exception e) {	
		}
		return lista;
	}
}
