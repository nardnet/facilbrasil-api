package com.facilbrasil.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.support.SpringBootServletInitializer;

import com.facilbrasil.api.config.property.FacilBrasilApiProperty;


@SpringBootApplication
@EnableConfigurationProperties(FacilBrasilApiProperty.class)
public class FacilbrasilApiApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(FacilbrasilApiApplication.class, args) ;
	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return  builder.sources(FacilbrasilApiApplication.class);
	}

	
}
