package com.facilbrasil.api.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.expression.OAuth2MethodSecurityExpressionHandler;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}
	
	
	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
		        .antMatchers("/atendimento").permitAll()
		        .antMatchers("/atendimento/*").permitAll() 
		        .antMatchers("/atendimento/mensagens/*").permitAll()
				.antMatchers("/clientes").permitAll()
				.antMatchers("/usuarios").permitAll()
				.antMatchers("/usuarios/usuarioCliente/*").permitAll()
				.antMatchers("/usuarios/atualizar").permitAll()
				.antMatchers("/usuarios/usuarioSistema").permitAll()
				.antMatchers("/cliente/titulos").permitAll()
				.antMatchers("/cliente/titulos/*").permitAll()
				.antMatchers("/usuarios/atualizar/*").permitAll()
				.antMatchers("/usuarios/*").permitAll()
				.antMatchers("/clientes/*").permitAll()	
				.antMatchers("/clientes/contrato/cliente").permitAll()
				.antMatchers("/clientes/visualizar/contrato").permitAll()
				.antMatchers("/clientes/titulos/cliente").permitAll()
				.antMatchers("/clientes/ativo/*").permitAll()
				.antMatchers("/titulos").permitAll()
				.antMatchers("/titulos/upload").permitAll()
				.antMatchers("/titulos/download").permitAll()	
				.antMatchers("/titulos/*").permitAll()	
				.antMatchers("/bancos").permitAll()
				.antMatchers("/estados").permitAll()
				.antMatchers("/tipodivida").permitAll()
				.anyRequest().authenticated()
				.and()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
			.csrf().disable();
	}
	
	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		resources.stateless(true);
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	public MethodSecurityExpressionHandler createExpressionHandler() {
		return new OAuth2MethodSecurityExpressionHandler();
	}

}
