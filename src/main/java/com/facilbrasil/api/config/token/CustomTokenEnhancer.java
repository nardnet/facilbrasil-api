package com.facilbrasil.api.config.token;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import com.facilbrasil.api.repository.ArquivoRepository;
import com.facilbrasil.api.repository.TitulosRepository;
import com.facilbrasil.api.security.UsuarioSistema;

public class CustomTokenEnhancer implements TokenEnhancer {
	
	static UsuarioSistema usuarioSistema;
	
	@Autowired
	TitulosRepository titulosRepository;
	
	@Autowired
	ArquivoRepository arquivoRepository;

	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		 usuarioSistema = (UsuarioSistema) authentication.getPrincipal();	 

		Map<String, Object> addInfo = new HashMap<>();		
		
		if (usuarioSistema.getUsuario().getCliente() != null) {
			addInfo.put("visivel", true);
			addInfo.put("usuario", usuarioSistema.getUsuario().getCliente().getNome());
			addInfo.put("codigoCliente", usuarioSistema.getUsuario().getCliente().getCodigo());
			addInfo.put("email", usuarioSistema.getUsuario().getCliente().getEmail());
			addInfo.put("saldoACobrar", titulosRepository.sumValorTitulos(usuarioSistema.getUsuario().getCliente()));
			addInfo.put("saldoCobrado", arquivoRepository.sumValorPago(usuarioSistema.getUsuario().getCliente().getCnpj()) );
		}else {
			addInfo.put("usuario", usuarioSistema.getUsuario().getUsuario());
			addInfo.put("email", usuarioSistema.getUsuario().getEmail());
		}
		
		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(addInfo);
		return accessToken;
	}

	public static UsuarioSistema getUsuarioSistema() {
		return usuarioSistema;
	}

	public static void setUsuarioSistema(UsuarioSistema usuarioSistema) {
		CustomTokenEnhancer.usuarioSistema = usuarioSistema;
	}
	
}
