package com.facilbrasil.api.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.facilbrasil.api.model.Estados;
import com.facilbrasil.api.repository.EstadosRepository;

@RestController
@RequestMapping("/estados")
public class EstadosResource {
	
	@Autowired
	private EstadosRepository estadosRepository;
	
	@GetMapping
	public List<Estados> listar() {
		return estadosRepository.findAll();
	}

}
