package com.facilbrasil.api.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.facilbrasil.api.model.TipoDivida;
import com.facilbrasil.api.repository.TipoDividaRepository;

@RestController
@RequestMapping("/tipodivida")
public class TipoDividaResource {

	@Autowired
	private TipoDividaRepository tipoDividaRepository;
	
	
	@GetMapping
	public List<TipoDivida> listar() {
		return tipoDividaRepository.findAll();
	}

	
}
