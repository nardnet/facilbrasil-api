package com.facilbrasil.api.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.facilbrasil.api.model.Bancos;
import com.facilbrasil.api.repository.BancosRepository;

@RestController
@RequestMapping("/bancos")
public class BancosResource {
	
	@Autowired
	private BancosRepository bancosRepository;
	
	@GetMapping
	public List<Bancos> listar() {
		return bancosRepository.findAll();
	}


}
