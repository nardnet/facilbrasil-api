package com.facilbrasil.api.resource;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.facilbrasil.api.event.RecursoCriadoEvent;
import com.facilbrasil.api.exceptionhandler.FacilbrasilExceptionHandler.Erro;
import com.facilbrasil.api.mail.Mailer;
import com.facilbrasil.api.model.Clientes;
import com.facilbrasil.api.model.Mensagem;
import com.facilbrasil.api.model.Titulos;
import com.facilbrasil.api.repository.MensagemRepository;
import com.facilbrasil.api.repository.TitulosRepository;
import com.facilbrasil.api.repository.filter.TituloFilter;
import com.facilbrasil.api.repository.projection.ResumoTitulos;
import com.facilbrasil.api.service.ClientesService;
import com.facilbrasil.api.service.TitulosService;
import com.facilbrasil.api.service.exception.ClienteInexistenteException;
import com.facilbrasil.api.service.exception.UploadAnexoException;
import com.facilbrasil.api.utils.ArquivoRet;
import com.facilbrasil.api.utils.PropertiesUtils;

@RestController
@RequestMapping("/titulos")
public class TitulosResource {
	
	final String remetente = "REMETENTE";
	final String coordenador = "COORDENADOR";
	final String tituloCriado = "mail/TitulosCliente";
	
	final String analista = "ANALISTA";
	final String erroFtpMail = "mail/erroFTP";
	
	@Autowired
	private PropertiesUtils prop;
	
	@Autowired
	private Mailer mail;
	
	@Autowired
	private TitulosRepository titulosRepository;
	
	@Autowired
	private TitulosService titulosServices;
	
	@Autowired
	private ClientesService clientesService;
	
	@Autowired
	private ArquivoRet arquivoRet;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private MensagemRepository mensagemRepository;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_TITULO') and #oauth2.hasScope('read')")
	public Page<Titulos> pesquisar(TituloFilter tituloFilter, Pageable pageable) {
		return titulosRepository.filtrar(tituloFilter,pageable);
	}
	
	@GetMapping(params = "resumo")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_TITULO') and #oauth2.hasScope('read')")
	public Page<ResumoTitulos> resumir(TituloFilter tituloFilter, Pageable pageable) {
		return titulosRepository.resumir(tituloFilter,pageable);
	}
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_TITULO') and #oauth2.hasScope('read')")
	public ResponseEntity<Titulos> buscarPeloCodigo(@PathVariable Long codigo) {
		Titulos titulos = titulosRepository.findOne(codigo);
		return titulos != null ? ResponseEntity.ok(titulos) : ResponseEntity.notFound().build();
	}
	
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_TITULO') and #oauth2.hasScope('write')")
	public ResponseEntity<Titulos> criar(@Valid @RequestBody Titulos titulos, HttpServletResponse response) {
		Titulos titulosSalvo = titulosServices.salvar(titulos);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, titulosSalvo.getCodigo()));
				
		return ResponseEntity.status(HttpStatus.CREATED).body(titulosSalvo);
	}
			
	
	@GetMapping(params ="nome")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_TITULO') and #oauth2.hasScope('read')")
	public Page<Titulos> pesquisar(@RequestParam(required = false, defaultValue = "%") String nome, Pageable pageable) {
		return titulosRepository.findByNomeContaining(nome, pageable);
	}
	
	
	@ExceptionHandler({ ClienteInexistenteException.class })
	public ResponseEntity<Object> handleClienteInexistenteException(ClienteInexistenteException ex) {
		String mensagemUsuario = messageSource.getMessage("cliente.inexistente", null, LocaleContextHolder.getLocale());
		String mensagemDesenvolvedor = ex.toString();
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		return ResponseEntity.badRequest().body(erros);
	}
	
	@DeleteMapping("/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PreAuthorize("hasAuthority('ROLE_REMOVER_TITULO') and #oauth2.hasScope('write')")
	public void remover(@PathVariable Long codigo) {
		titulosRepository.delete(codigo);
	}
	
	/*@PutMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_TITULO') and #oauth2.hasScope('write') ")
	public ResponseEntity<Titulos> atualizar(@PathVariable Long codigo, @Valid @RequestBody Titulos titulo) {
		try {
			Titulos tituloSalvo = titulosServices.atualizar(codigo, titulo);
			return ResponseEntity.ok(tituloSalvo);
		} catch (IllegalArgumentException e) {
			return ResponseEntity.notFound().build();
		}
	}*/
	
	@PostMapping("/upload")	
	public ResponseEntity<String> uploadAnexo(@RequestParam("codigo") Long codigo, @RequestParam("quantidade") Long quantidade, 
			@RequestParam("saldoACobrar") BigDecimal saldoACobrar, @RequestParam("file") MultipartFile upload) throws IOException {
		
		if (upload.isEmpty())
		throw new UploadAnexoException();
		
		Clientes cliente = clientesService.buscarClientePeloCodigo(codigo);
		
		if (cliente == null) {
			throw new ClienteInexistenteException();
		}
		
		arquivoRet.trasferArquivos(quantidade, saldoACobrar, cliente, upload);
	
		return new ResponseEntity<>(
			      "Importação concluida com sucesso!", 
			      HttpStatus.OK);
	}
	
	@ExceptionHandler({ UploadAnexoException.class })
	public ResponseEntity<Object> handleUploadException(UploadAnexoException ex) {
		String mensagemUsuario = messageSource.getMessage("anexo.erro.importacao", null, LocaleContextHolder.getLocale());
		String mensagemDesenvolvedor = ex.toString();
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		return ResponseEntity.badRequest().body(erros);
	}
	
	@GetMapping("/download")
	public ResponseEntity<byte[]> download() throws Exception {
    	InputStream inputStream = this.getClass().getResourceAsStream(
				"/modelo/Modelo_Importação.xlsx");

        byte[] arquivo = new byte[inputStream.available()];
        inputStream.read(arquivo);
        
        
      return  ResponseEntity.ok()
        .header(HttpHeaders.CONTENT_DISPOSITION,
              "attachment;filename=" + "Modelo_Importação.xlsx")
        .header("Access-Control-Allow-Credentials", "true")
        .header("Access-Control-Expose-Headers", "Content-Disposition, filename")
        .header("filename", "Modelo_Importação.xlsx")
        .contentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
        .body(arquivo); 
	}
	
	@PutMapping("/{codigo}")
	public ResponseEntity<Clientes> atualizar(@PathVariable Long codigo, @Valid @RequestBody Clientes cliente) {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Date hoje = Calendar.getInstance().getTime();
		String dataAtual = df.format(hoje);
		Clientes clienteSalvo = clientesService.atualizarTitulos(codigo, cliente);
		clientesService.exportarXls(clienteSalvo);	
		
		
		List<Titulos> titulos = new ArrayList<Titulos>();
		
		for (Titulos titulo :  clienteSalvo.getTitulos()) {
            String dataEnvio = df.format(titulo.getDataEnvioEntrada());
			if (dataAtual.equals(dataEnvio)) {
				titulos.add(titulo);
			}			
		 }
		
		
		    Map<String, Object> variaveis = new HashMap<>();
	 		variaveis.put("clienteSalvo", clienteSalvo);
	 		variaveis.put("titulos", titulos);
	 		
	 		mail.enviarEmail(prop.getLoadconfigBanco(remetente), 
	 				Arrays.asList(prop.getLoadconfigBanco(remetente),prop.getLoadconfigBanco(coordenador),clienteSalvo.getEmail()), 
	 				"Olá "+ clienteSalvo.getNome()  +". Seus títulos foram confirmados no Sistema da Fácil Brasil!", tituloCriado, variaveis);
	 		System.out.println("Terminado o envio de e-mail...");
		
			Mensagem mensagem = new Mensagem();
			
			mensagem.setCodigoCliente(clienteSalvo.getCodigo());
			mensagem.setAssunto("Olá "+ clienteSalvo.getNome()  +". Seus títulos foram confirmados no Sistema da Fácil Brasil!");
						
									   
						   String msg =  "Olá, " + clienteSalvo.getNome()  + " " 

                           + "\n Seus títulos foram criados!  "
						   + "\n Obrigado. "
						   + "\n Equipe Fácil Brasil! ";
						
			mensagem.setMensagem(msg);
			mensagem.setNome(clienteSalvo.getNome());
			mensagem.setEmail(clienteSalvo.getEmail());
			mensagem.setInterno(true);
						
			mensagemRepository.save(mensagem);
		
		return ResponseEntity.ok(clienteSalvo);
	}	
	
}
