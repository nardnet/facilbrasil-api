package com.facilbrasil.api.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.facilbrasil.api.event.RecursoCriadoEvent;
import com.facilbrasil.api.model.Usuario;
import com.facilbrasil.api.repository.UsuarioRepository;
import com.facilbrasil.api.service.UsuarioService;

@RestController
@RequestMapping("/usuarios")
public class UsuarioResource {
	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@GetMapping
	public List<Usuario> listar() {
		return usuarioRepository.findAll();
	}
	
	@GetMapping("/usuarioSistema")
	public List<Usuario> buscarUsuarioSemCliente() {
		List<Usuario> usuarios = usuarioRepository.findUsuarioSemCliente();		 
		 return usuarios;
	}
	
	@PostMapping
	public ResponseEntity<Usuario> criar(@Valid @RequestBody Usuario usuario, HttpServletResponse response) {
		String senha = usuario.getSenha();	
		usuario.setLembrete(senha);
		usuario.setSenha(encoder.encode(senha));  
		Usuario usuarioSalvo = usuarioRepository.save(usuario);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, usuarioSalvo.getCodigo()));
				
		return ResponseEntity.status(HttpStatus.CREATED).body(usuarioSalvo);
	}
	
	@GetMapping("/usuarioCliente/{codigo}")
	public ResponseEntity<Usuario> buscarPeloCodigoCliente(@PathVariable Long codigo) {
		Usuario usuario = usuarioRepository.findUsuarioPorCliente(codigo);		 
		 return usuario != null ? ResponseEntity.ok(usuario) : ResponseEntity.notFound().build();
	}
	
	@GetMapping("/{codigo}")
	public ResponseEntity<Usuario> buscarPeloCodigo(@PathVariable Long codigo) {
		Usuario usuario = usuarioRepository.findOne(codigo);		 
		 return usuario != null ? ResponseEntity.ok(usuario) : ResponseEntity.notFound().build();
	}
	
	@DeleteMapping("/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long codigo) {
		usuarioRepository.delete(codigo);
	}
	
	@PutMapping("/{codigo}")
	public ResponseEntity<Usuario> atualizar(@PathVariable Long codigo, @Valid @RequestBody Usuario usuario) {	
		Usuario usuarioSalvo = usuarioService.atualizar(codigo, usuario);
		return ResponseEntity.ok(usuarioSalvo);
	}
	
	/*@PutMapping("/atualizar")	
	public ResponseEntity<Usuario> atualizar(@Valid @RequestBody Usuario usuario) {	
		List<Permissao> listaPermissao = new ArrayList<Permissao>();
		Permissao permissao = new Permissao();
        permissao.setCodigo((long)1);
        listaPermissao.add(permissao);
	    usuario.setPermissoes(listaPermissao);		
		Usuario usuarioSalvo = usuarioService.atualizar(usuario);
		return ResponseEntity.ok(usuarioSalvo);
	}*/

}
