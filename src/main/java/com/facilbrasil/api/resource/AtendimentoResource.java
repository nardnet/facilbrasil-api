package com.facilbrasil.api.resource;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.facilbrasil.api.event.RecursoCriadoEvent;
import com.facilbrasil.api.mail.Mailer;
import com.facilbrasil.api.model.AtendimentoContato;
import com.facilbrasil.api.model.Mensagem;
import com.facilbrasil.api.repository.AtendimentoContatoRepository;
import com.facilbrasil.api.repository.MensagemRepository;
import com.facilbrasil.api.utils.PropertiesUtils;



@RestController
@RequestMapping("/atendimento")
public class AtendimentoResource {
	final String remetente = "REMETENTE";
	final String coordenador = "COORDENADOR";
	
	@Autowired
	private PropertiesUtils prop;
	
	final String template = "mail/bem-vindo";
	final String atendimentoMensagem = "mail/AtendimentoMensagem";	
	final String atendimentoContato = "mail/AtendimentoContato";	
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@Autowired
	private MensagemRepository mensagemRepository;
	
	@Autowired
	private AtendimentoContatoRepository atendimentoRepository;

		
	@Autowired
	private Mailer mail;
	
	
	@PostMapping("/mensagem")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Mensagem> criarMensagem(@Valid @RequestBody Mensagem mensagem, HttpServletResponse response) {

		Mensagem mensagemSalvo = mensagemRepository.save(mensagem);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, mensagemSalvo.getCodigo()));
		
		 Map<String, Object> variaveis = new HashMap<>();
		 		variaveis.put("mensagemSalvo", mensagemSalvo);
		 		
		 		 mail.enviarEmail(prop.getLoadconfigBanco(remetente), 
			 				Arrays.asList(prop.getLoadconfigBanco(remetente),prop.getLoadconfigBanco(coordenador)), 
			 				"Atenção -Solicitação de Atendimento via Mensagem Fácil Brasil!", atendimentoMensagem, variaveis);
			 		System.out.println("Terminado o envio de e-mail...");
		 		
		 		
		return ResponseEntity.status(HttpStatus.CREATED).body(mensagemSalvo);
	
	}
	
	@GetMapping("/mensagens/{codigo}")
	public ResponseEntity<List<Mensagem>> buscarPeloCodigo(@PathVariable Long codigo) {
		List<Mensagem> Mensagem =  mensagemRepository.findMensagens(codigo);
		
		
		System.out.println("Entrou");
	   
		return  ResponseEntity.ok(Mensagem);
	}
	
	@PostMapping("/contato")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<AtendimentoContato> criarContato(@Valid @RequestBody AtendimentoContato contato, HttpServletResponse response) {

		AtendimentoContato contatoSalvo = atendimentoRepository.save(contato);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, contatoSalvo.getCodigo()));
		
		 Map<String, Object> variaveis = new HashMap<>();
		 		variaveis.put("contatoSalvo", contatoSalvo);
		 		
		 		 mail.enviarEmail(prop.getLoadconfigBanco(remetente), 
			 				Arrays.asList(prop.getLoadconfigBanco(remetente),prop.getLoadconfigBanco(coordenador)), 
			 				"Atenção - Solicitação de Atendimento via Ligamos para Você da Fácil Brasil!", atendimentoContato, variaveis);
			 		System.out.println("Terminado o envio de e-mail...");
		 		
		 		
		return ResponseEntity.status(HttpStatus.CREATED).body(contatoSalvo);
	
	}
	
}
