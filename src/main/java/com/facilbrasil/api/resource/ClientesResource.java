package com.facilbrasil.api.resource;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.facilbrasil.api.exceptionhandler.FacilbrasilExceptionHandler.Erro;
import com.facilbrasil.api.mail.Mailer;
import com.facilbrasil.api.model.Clientes;
import com.facilbrasil.api.model.Mensagem;
import com.facilbrasil.api.model.Usuario;
import com.facilbrasil.api.repository.ClientesRepository;
import com.facilbrasil.api.repository.MensagemRepository;
import com.facilbrasil.api.repository.UsuarioRepository;
import com.facilbrasil.api.repository.filter.ClienteFilter;
import com.facilbrasil.api.repository.projection.ResumoCliente;
import com.facilbrasil.api.service.ClientesService;
import com.facilbrasil.api.service.exception.ClienteExistenteException;
import com.facilbrasil.api.service.exception.ClienteInexistenteException;
import com.facilbrasil.api.utils.PropertiesUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/clientes")
public class ClientesResource {
	final String remetente = "REMETENTE";
	final String coordenador = "COORDENADOR";
	
	@Autowired
	private PropertiesUtils prop;
	
	final String template = "mail/bem-vindo";
	final String novoCliente = "mail/NovoCliente";
	final String acessoCliente = "mail/AcessoACliente";
	
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private ClientesRepository clientesRepository;
	
	@Autowired
	private ClientesService clientesService;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private MensagemRepository mensagemRepository;
	
	@Autowired
	private Mailer mail;

	
	@GetMapping	
	public Page<Clientes> pesquisar(ClienteFilter clienteFilter, Pageable pageable) {
		return clientesRepository.filtrar(clienteFilter,pageable);
	}
	
	@GetMapping(params = "resumo")	
	public Page<ResumoCliente> resumir(ClienteFilter clienteFilter, Pageable pageable) {
		return clientesRepository.resumir(clienteFilter,pageable);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Clientes> criar(@Valid @RequestBody Clientes clientes, HttpServletResponse response) {
		clientes.setDtCadastro(new Date());
		
		
		if (!StringUtils.isEmpty(clientes.getCnpj())) {
			Clientes clienteSalvo = clientesRepository.getClienteByCnpj(clientes.getCnpj());
			
			if (clienteSalvo != null) {
				throw new ClienteExistenteException();
			}
			
		}
		
       	Clientes clienteSalvo = clientesService.salvar(clientes);
	//	publisher.publishEvent(new RecursoCriadoEvent(this, response, clienteSalvo.getCodigo()));
		
		 Map<String, Object> variaveis = new HashMap<>();
		 		variaveis.put("clienteSalvo", clienteSalvo);
		 		
		 		 mail.enviarEmail(prop.getLoadconfigBanco(remetente), 
		 				Arrays.asList(clienteSalvo.getEmail()), 
		 				"Bem vindo ao nosso time!", template, variaveis);
		 		System.out.println("Terminado o envio de e-mail...");
		 		
		 		 mail.enviarEmail(prop.getLoadconfigBanco(remetente), 
			 				Arrays.asList(prop.getLoadconfigBanco(remetente),prop.getLoadconfigBanco(coordenador)), 
			 				"Nova adesão a parceria Fácil Brasil!", novoCliente, variaveis);
			 		System.out.println("Terminado o envio de e-mail...");
			 		
		Mensagem mensagem = new Mensagem();
					
		mensagem.setCodigoCliente(clienteSalvo.getCodigo());
		mensagem.setAssunto("Bem vindo ao nosso time!");
					
					   String msg = "Olá, " + clienteSalvo.getNome()
						     + "\n Razão Social: " + clienteSalvo.getRzoSocial()
							 + "\n CNPJ: " + clienteSalvo.getCnpj() 
							
							 + "\n Estamos felizes com o seu cadastro"
							 + "\n Aguarde o email do setor comercial com seu login para acesso a área do cliente"
							
	                         + "\n Desde já, a Fácil Brasil agradece a sua confiança e preferência em nosso trabalho. "
							 + "\n Você tomou a decisão certa! "
						
							 + "\n Fácil Brasil Contact Center";
					
		mensagem.setMensagem(msg);		
		mensagem.setNome(clienteSalvo.getNome());
		mensagem.setEmail(clienteSalvo.getEmail());
		mensagem.setInterno(true);
					
		mensagemRepository.save(mensagem);
		 		
		 		
		return ResponseEntity.status(HttpStatus.CREATED).body(clienteSalvo);

	}

	@GetMapping("/{codigo}")
	public ResponseEntity<Clientes> buscarPeloCodigo(@PathVariable Long codigo) {
		 Clientes clientes = clientesRepository.findOne(codigo);		 
		 return clientes != null ? ResponseEntity.ok(clientes) : ResponseEntity.notFound().build();
	}
	
	@DeleteMapping("/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PreAuthorize("hasAuthority('ROLE_REMOVER_CLIENTE') and #oauth2.hasScope('write')")
	public void remover(@PathVariable Long codigo) {
		clientesRepository.delete(codigo);
	}
		
	@GetMapping("/contrato/cliente")
	public ResponseEntity<byte[]> relatorioCliente(@RequestParam Long codigo) throws Exception {
		Clientes cliente = clientesRepository.findOne(codigo);
		
        byte[] relatorio = clientesService.relatorioContrato(cliente);
		
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE)
				.body(relatorio);
		
	}	
	
	@PostMapping("/visualizar/contrato")
	public ResponseEntity<byte[]> gerarContrato(@RequestBody String objetoJson)throws Exception {
		
		Clientes cliente = new ObjectMapper().readValue(objetoJson, Clientes.class);
		
        byte[] relatorio =  clientesService.relatorioContrato(cliente);
		
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE)
				.body(relatorio);
		
	}
	
	@GetMapping("/titulos/cliente")
	public ResponseEntity<byte[]> titulosCliente(@RequestParam Long codigo) throws Exception {
		Clientes cliente = clientesRepository.findOne(codigo);
		String arquivo = "titulos_"+ cliente.getNome() + ".xls";
		ByteArrayOutputStream output = clientesService.gerarXls(cliente);	
		byte[] bytes = output.toByteArray();
		return ResponseEntity.ok()
				 .header("Content-Disposition", "attachment; filename=" +arquivo)
	            .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
				.body(bytes);
		
	}	
			
	@PutMapping("/{codigo}")
	public ResponseEntity<Clientes> atualizar(@PathVariable Long codigo, @Valid @RequestBody Clientes cliente) {	
		Clientes clienteSalvo = clientesService.atualizar(codigo, cliente);
		Usuario usuario = usuarioRepository.findUsuarioPorCliente(clienteSalvo.getCodigo());
		
		 Map<String, Object> variaveis = new HashMap<>();
	 		variaveis.put("clienteSalvo", clienteSalvo);
	 		variaveis.put("usuario", usuario);
	 		
		 mail.enviarEmail(prop.getLoadconfigBanco(remetente), 
	 				Arrays.asList(clienteSalvo.getEmail()), 
	 				"Seu acesso FácilBrasil está pronto!", acessoCliente, variaveis);
	 		System.out.println("Terminado o envio de e-mail...");
	 		
	 		
			Mensagem mensagem = new Mensagem();
			
			mensagem.setCodigoCliente(clienteSalvo.getCodigo());
			mensagem.setAssunto("Seu acesso FácilBrasil está pronto!");

						  String msg = "Olá, " + clienteSalvo.getNome() 
						   
                     
                             + "\n  Agora você já pode acessar a 'Área do Cliente' da Fácil Brasil! "
                             + "\n Para acessar utilize os dados fornecidos abaixo: "
                      
                             + "\n http://cadastro.facilbrasil.com/ "
                         
                             + "\n Entre com o seu usuário e senha abaixo: "
                             + "\n Usuário: " + usuario.getUsuario()
                             + "\n Senha: " + usuario.getLembrete()
						     + "\n Atenciosamente, "
						     + "\n Fácil Brasil Contact Center";
						
			mensagem.setMensagem(msg);		
			mensagem.setNome(clienteSalvo.getNome());
			mensagem.setEmail(clienteSalvo.getEmail());
			mensagem.setInterno(true);
						
			mensagemRepository.save(mensagem);	
	 		
	 		
	 		
		return ResponseEntity.ok(clienteSalvo);
	}
	
	
	@PutMapping("/ativo/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void atualizarPropriedadeAtivo(@PathVariable Long codigo, @RequestBody Boolean ativo) {
		clientesService.atualizarPropriedadeAtivo(codigo, ativo);
	}
	
	@ExceptionHandler({ ClienteInexistenteException.class })
	public ResponseEntity<Object> handleClienteInexistenteOuInativaException(ClienteInexistenteException ex) {
		String mensagemUsuario = messageSource.getMessage("cliente.inexistente", null, LocaleContextHolder.getLocale());
		String mensagemDesenvolvedor = ex.toString();
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		return ResponseEntity.badRequest().body(erros);
	}
	
	
	@ExceptionHandler({ ClienteExistenteException.class })
	public ResponseEntity<Object> handleClienteInexistenteOuInativaException(ClienteExistenteException ex) {
		String mensagemUsuario = messageSource.getMessage("cliente.existente", null, LocaleContextHolder.getLocale());
		String mensagemDesenvolvedor = ex.toString();
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		return ResponseEntity.badRequest().body(erros);
	}
	
	
	
	
}
