package com.facilbrasil.api.mail;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;

/*
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;*/

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/*import com.facilbrasil.api.model.Clientes;
import com.facilbrasil.api.repository.ClientesRepository;*/

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.facilbrasil.api.model.Titulos;
import com.facilbrasil.api.repository.TitulosRepository;
import com.facilbrasil.api.utils.ArquivoRet;
import com.facilbrasil.api.utils.PropertiesUtils;

@Component
@EnableScheduling
public class Mailer {
	final String remetente = "REMETENTE";
	final String coordenador = "COORDENADOR";
	final String tituloCriado = "mail/TituloCriado";
	@Autowired
	private PropertiesUtils prop;
	
	@Autowired
	private ArquivoRet arquivos;
	
	@Autowired
	private TitulosRepository titulosReposity;
	
	@Autowired
	private JavaMailSender mailSender;
		
	@Autowired
	private TemplateEngine thymeleaf;
/*	
	@Autowired
	private ClientesRepository repo;*/
	
	/*@EventListener
	private void teste(ApplicationReadyEvent event) {
		String template = "mail/bem-vindo";
		
		Clientes clienteSalvo = repo.findOne((long) 1);
		Map<String, Object> variaveis = new HashMap<>();
		variaveis.put("clienteSalvo", clienteSalvo);
		
		this.enviarEmail("nardnet@gmail.com", 
				Arrays.asList("nardnet@gmail.com"), 
				"Testando", template, variaveis);
		System.out.println("Terminado o envio de e-mail...");
	}	
	
	
	@EventListener
	private void teste(ApplicationReadyEvent event) {
		this.enviarEmail(prop.getLoadconfigBanco(remetente), 
				Arrays.asList("nardnet@gmail.com"), 
		"Testando", "Olá!<br/>Teste ok 1.");
		System.out.println("Terminado o envio de e-mail...");
	}
	*/
	
	@Scheduled(cron = "1 1 18 * * *")
	public void avisarSobreTitulosCriados() {
		List<Titulos> titulos = new ArrayList<Titulos>();
		Date dataAtual = new Date();
		titulos = titulosReposity.findByTitulosHoje(dataAtual);
		
		if (titulos != null && titulos.size() > 0) {
			 Map<String, Object> variaveis = new HashMap<>();
		 		variaveis.put("titulos", titulos);
		 		
		 		 this.enviarEmail(prop.getLoadconfigBanco(remetente), 
		 				Arrays.asList(prop.getLoadconfigBanco(remetente),prop.getLoadconfigBanco(coordenador)), 
		 				"Titulos de hoje!", tituloCriado, variaveis);
		 		System.out.println("Terminado o envio de e-mail...");
			System.out.println(">>>>>>>>>>>>>>> Método sendo executado...");
		}
		

	}
	
	//@Scheduled(cron = "0/10 * * * * *")	
	@Scheduled(cron = "0 0/5 * * * *")
	public void importarArquivosRet() {
		
		
		System.out.println(">>>>>>>>>>>>>>>INICIO IMPORTAR ARQUIVOS.");
		try {
			arquivos.importarArquivos();
		} catch (IOException e) {
			System.out.println(">>>>>>>>>>>>>>>ERRO NA IMPORTAÇÃO DE ARQUIVOS.");
			e.printStackTrace();
		}
        arquivos.deletarArquivos();	
		System.out.println(">>>>>>>>>>>>>>>TERMINO IMPORTAR ARQUIVOS.");
	}
	
		
	public void enviarEmail(String remetente, 
			List<String> destinatarios, String assunto, String template, 
			Map<String, Object> variaveis) {
		Context context = new Context(new Locale("pt", "BR"));
		
		variaveis.entrySet().forEach(
				e -> context.setVariable(e.getKey(), e.getValue()));
		
		String mensagem = thymeleaf.process(template, context);
		
		this.enviarEmail(remetente, destinatarios, assunto, mensagem);
	}
	
	public void enviarEmail(String remetente, 
			List<String> destinatarios, String assunto, String mensagem) {
		try {
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "UTF-8");
			helper.setFrom(remetente);
			helper.setTo(destinatarios.toArray(new String[destinatarios.size()]));
			helper.setSubject(assunto);
			helper.setText(mensagem, true);
			
			mailSender.send(mimeMessage);
		} catch (MessagingException e) {
			throw new RuntimeException("Problemas com o envio de e-mail!", e); 
		}
	}
}
